## git部署公钥指南

执行此命令，会生成公钥和私钥两个文件

```
 ssh-keygen -t rsa
```

执行此命令，进入目录

```
 cd /root/.ssh/
```

执行此命令，查看公钥

```
 cat id_rsa.pub
```

复制公钥信息


登录码云-点击设置-找到安全设置下的ssh公钥-把复制的公钥粘贴到公钥出，点击确定按钮


安全连接建立完成