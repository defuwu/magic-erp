# 基础设施部署

### 前言

> **一**、基础设施请基于Docker安装部署

> **二**、请注意关闭防火墙或开放相关端口

关闭防火墙

```
systemctl stop firewalld &&\
systemctl disable firewalld
```

### 安装Docker

```bash
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
```

```bash
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

```bash
sudo yum install -y docker-ce-18.03.1.ce
```

设置开机自启，并启动docker

```bash
systemctl enable docker
systemctl start docker
```

### 部署RabbitMQ

##### 1.建立数据存储目录

```bash
mkdir -p /opt/data/mqdata
chmod -R 777 /opt/data/mqdata
```

##### 2.运行mq

> 安全起见，请修改上述 RABBITMQ_ERLANG_COOKIE 的值（任意字串）
>
> 默认用户名密码是guest/guest
>
> 建议修改guest的密码，且创建自己的vhost和用户

```bash
docker run --rm -d --hostname rabbit \
 -p 15672:15672  -p 5672:5672 -p 25672:25672 -p 4369:4369 -p 35672:35672 \
 -v /opt/data/mqdata:/var/lib/rabbitmq \
 -e RABBITMQ_ERLANG_COOKIE='MY-SECRET-KEY' \
 --name rabbitmq   registry.cn-beijing.aliyuncs.com/shoptnt/rabbitmq:3.8.9
```

### 部署Redis

> 注意修改redis的密码，如果不想要密码，可以删除命令末尾的 --requirepass "123456"

##### 1.建立数据存储目录

```bash
mkdir -p /opt/data/redisdata
chmod -R 777 /opt/data/redisdata
```

##### 2.运行redis

```bash
docker run --rm -d -p 6379:6379 \
-v /opt/data/redisdata:/data \
--name redis registry.cn-beijing.aliyuncs.com/shoptnt/redis:6.0.10 \
redis-server --appendonly yes --requirepass "123456"
```

### 部署MySQL

> 如果您使用阿里云数据库，则可以跳过此步骤
>
> 如您需要使用本地安装MySQL，请执行以下步骤

##### 1.建立数据存储目录

```bash
mkdir -p /home/mysql/{conf,logs,data/mysql}
```

```bash
vi /home/mysql/conf/my.cnf
```

将粘贴以下内容

```
[mysqld]
pid-file        = /var/run/mysqld/mysqld.pid
socket          = /var/run/mysqld/mysqld.sock
datadir         = /var/lib/mysql
secure-file-priv= NULL
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
lower_case_table_names=1
# Custom config should go here
!includedir /etc/mysql/conf.d/
```

##### 2.运行MySQL

> 请修改 MYSQL_ROOT_PASSWORD 的值来修改root的密码

```bash
docker run --rm -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 --name mysql -v /home/mysql/conf/my.cnf:/etc/mysql/my.cnf -v /home/mysql/logs:/logs -v /home/mysql/data/mysql:/var/lib/mysql   registry.cn-beijing.aliyuncs.com/shoptnt/mysql:5.6.35
```

##### 3.创建数据库

```
CREATE DATABASE `erp`
```

##### 4.安装数据

* 执行项目源码中 sql 文件夹下的 database.sql 文件，进行建表以及安装基础数据

### 部署xxl-job



> 在您部署的MySQL中建立xxl-job的数据库


```
CREATE DATABASE `xxl_job`
```



>在xxl-job数据库中执行项目源码中 sql 文件夹下的 xxl_job.sql 文件，进行建表以及安装基础数据

```bash
docker run --rm -e PARAMS="--spring.datasource.url=jdbc:mysql://ip:3306/xxl_job?Unicode=true&characterEncoding=UTF-8  --spring.datasource.username=root --spring.datasource.password=123456" -p 8080:8080 -v /tmp:/data/applogs --name xxl-job-admin  -d  registry.cn-beijing.aliyuncs.com/shoptnt/xxl-job-admin:2.2.0
```


> 请修改相应密码为在"安装数据"步骤中设定的xxl-job数据库的密码
>
> 请注意数据ip使用实际ip,不要使用127.0.0.1等字
>
> 注意：这里的密码不支持特殊字符


如果要设置xxl-job的账号和密码

可以在docker启动参数的PARAMS中添加

```yaml
 --xxl.job.login.username=用户名 
 --xxl.job.login.password=密码
```

> 2.2.0版本默认的账户是admin，密码是123456