package com.hys.app.framework.apptoken;

import com.hys.app.framework.apptoken.model.AppTypeEnum;

/**
 * access token唯一标识符<br/>
 * 这个标识符会用来识别token getter、缓存token，查找token<br/>
 * 应该保证其中build id的唯一性<b/>
 * 这个抽象的标识符，包含了一个AccesssTokenAppEnum，即标识了由哪个token getter获取token.
 * @author kingapex
 * @data 2021/8/29 12:37
 * @version 1.0
 **/
public  abstract class AccessTokenIdentifier {


    /**
     * 第三方应用
     */
    private AppTypeEnum appTypeEnum;


    public AccessTokenIdentifier(AppTypeEnum appTypeEnum) {
        this.appTypeEnum = appTypeEnum;
    }

    public AppTypeEnum getAppEnum() {
        return this.appTypeEnum;
    }

    public abstract String buildId();

}
