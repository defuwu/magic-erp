package com.hys.app.framework.apptoken.model;

/**
 * app 类型 枚举
 * 获取access token的会根据这个枚举识别token getter
 * 以便调用相应的第三方应用api。
 * 如微信，企业微信，支付宝等
 * @author kingapex
 * @data 2021/8/29 12:56
 * @version 1.0
 **/
public enum AppTypeEnum {

    WeiXin,
    QiyeWeiXin

}
