package com.hys.app.framework.context.user;

import com.hys.app.framework.security.model.Clerk;
import com.hys.app.framework.security.model.Guide;

/**
 * 用户上下文
 * Created by kingapex on 2018/3/12.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/12
 */
public class ChainUserContext {


    /**
     * 获取导购
     */
    public static Guide getGuide() {
        return (Guide) UserContext.getSeller();
    }

    /**
     * 获取收银员
     */
    public static Clerk getCashier() {
        return (Clerk) UserContext.getSeller();
    }


}
