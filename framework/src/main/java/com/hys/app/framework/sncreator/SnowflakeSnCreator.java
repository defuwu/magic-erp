package com.hys.app.framework.sncreator;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Snowflake 实现的发号器
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-11-22
 */

@Configuration
public class SnowflakeSnCreator implements SnCreator {

    @Autowired
    private IdentifierGenerator keyGenerator;

    /**
     *最大递增序列
     */
    private static final long MAX_SEQ = 4000L;

    /**
     * 起始时间戳
     */
    private static final long START_TIME = 1587092627355L;

    private static long COUNT =0;

    /**
     * 获取递增序列
     * @return
     */
    private  synchronized long getSequence() {
        COUNT++;

        if (COUNT > MAX_SEQ) {
            COUNT =0;
        }

        return COUNT;
    }


    @Override
    public Long create(int subCode) {

        return  keyGenerator.nextId(new Object()).longValue();
    }


}
