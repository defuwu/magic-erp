package com.hys.app.controller.erp.basic;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.NoGenerateRuleQueryParams;
import com.hys.app.model.erp.vo.NoGenerateRuleVO;
import com.hys.app.model.erp.dto.NoGenerateRuleDTO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.NoGenerateRuleManager;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 编号生成规则相关API
 *
 * @author 张崧
 * 2023-12-01 11:37:56
 */
@RestController
@RequestMapping("/admin/erp/noGenerateRule")
@Api(tags = "编号生成规则相关API")
@Validated
public class NoGenerateRuleManagerController {

    @Autowired
    private NoGenerateRuleManager noGenerateRuleManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<NoGenerateRuleVO> list(NoGenerateRuleQueryParams queryParams) {
        return noGenerateRuleManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "添加名称为[${noGenerateRuleDTO.name}]的编号生成规则")
    public void add(@RequestBody @Valid NoGenerateRuleDTO noGenerateRuleDTO) {
        noGenerateRuleManager.add(noGenerateRuleDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改ID为[${id}]的编号生成规则")
    public void edit(@PathVariable Long id, @RequestBody @Valid NoGenerateRuleDTO noGenerateRuleDTO) {
        noGenerateRuleDTO.setId(id);
        noGenerateRuleManager.edit(noGenerateRuleDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public NoGenerateRuleVO getDetail(@PathVariable Long id) {
        return noGenerateRuleManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除ID为${ids}的编号生成规则")
    public void delete(@PathVariable List<Long> ids) {
        noGenerateRuleManager.delete(ids);
    }
}

