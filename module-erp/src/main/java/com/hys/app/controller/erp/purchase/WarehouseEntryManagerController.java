package com.hys.app.controller.erp.purchase;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.WarehouseEntryDTO;
import com.hys.app.model.erp.dto.WarehouseEntryQueryParams;
import com.hys.app.model.erp.dto.WarehouseEntryStatisticsParam;
import com.hys.app.model.erp.enums.WarehouseEntryStatusEnum;
import com.hys.app.model.erp.vo.WarehouseEntryVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.WarehouseEntryManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 入库单相关API
 *
 * @author 张崧
 * @since 2023-12-05 11:25:07
 */
@RestController
@RequestMapping("/admin/erp/warehouseEntry")
@Api(tags = "入库单相关API")
@Validated
public class WarehouseEntryManagerController {

    @Autowired
    private WarehouseEntryManager warehouseEntryManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<WarehouseEntryVO> list(WarehouseEntryQueryParams queryParams) {
        return warehouseEntryManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "创建入库单")
    public void add(@RequestBody @Valid WarehouseEntryDTO warehouseEntryDTO) {
        warehouseEntryManager.add(warehouseEntryDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "编辑ID为[${id}]的入库单")
    public void edit(@PathVariable Long id, @RequestBody @Valid WarehouseEntryDTO warehouseEntryDTO) {
        warehouseEntryDTO.setId(id);
        warehouseEntryManager.edit(warehouseEntryDTO);
    }

    @ApiOperation(value = "提交入库单")
    @PostMapping("/{ids}/submit")
    @Log(client = LogClient.admin, detail = "提交ID为[${ids}]的入库单")
    public void submit(@PathVariable List<Long> ids) {
        warehouseEntryManager.submit(ids);
    }

    @ApiOperation(value = "撤销提交入库单")
    @PostMapping("/{id}/withdraw")
    @Log(client = LogClient.admin, detail = "撤销提交ID为[${id}]的入库单")
    public void withdraw(@PathVariable Long id) {
        warehouseEntryManager.withdraw(id);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public WarehouseEntryVO getDetail(@PathVariable Long id) {
        return warehouseEntryManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的入库单")
    public void delete(@PathVariable List<Long> ids) {
        warehouseEntryManager.delete(ids);
    }

    @ApiOperation(value = "审核入库单")
    @PostMapping("/{ids}/audit")
    @Log(client = LogClient.admin, detail = "审核ID为[${ids}]的入库单")
    public void audit(@PathVariable List<Long> ids, @NotNull WarehouseEntryStatusEnum status, String remark) {
        warehouseEntryManager.audit(ids, status, remark);
    }

    @ApiOperation(value = "查询入库单统计分页列表数据")
    @GetMapping("/statistics")
    public WebPage statistics(WarehouseEntryStatisticsParam params) {
        return warehouseEntryManager.statistics(params);
    }

    @ApiOperation(value = "导出入库单统计列表数据")
    @GetMapping("/export")
    public void export(HttpServletResponse response, WarehouseEntryStatisticsParam params) {
        this.warehouseEntryManager.export(response, params);
    }
}

