package com.hys.app.controller.erp.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StockStatisticsQueryParam;
import com.hys.app.service.erp.StockStatisticsManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * 库存统计相关API
 *
 * @author dmy
 * 2023-12-05
 */
@Api(description = "库存统计相关API")
@RestController
@RequestMapping("/admin/stock/statistics")
@Validated
public class StockStatisticsManagerController {

    @Autowired
    private StockStatisticsManager stockStatisticsManager;

    @ApiOperation(value = "查询库存统计分页列表数据")
    @GetMapping
    public WebPage list(StockStatisticsQueryParam params) {
        return stockStatisticsManager.listStock(params);
    }

    @ApiOperation(value = "查询库存成本统计分页列表数据")
    @GetMapping("/cost")
    public WebPage listCost(StockStatisticsQueryParam params) {
        params.setGroupByWarehouseEntry(true);
        return stockStatisticsManager.listStock(params);
    }

    @ApiOperation(value = "导出库存统计列表数据")
    @GetMapping("/export")
    public void export(HttpServletResponse response, StockStatisticsQueryParam params) {
        this.stockStatisticsManager.export(response, params);
    }

    @ApiOperation(value = "导出库存成本统计列表数据")
    @GetMapping("/export-cost")
    public void exportCost(HttpServletResponse response, StockStatisticsQueryParam params) {
        this.stockStatisticsManager.exportCost(response, params);
    }
}
