package com.hys.app.controller.erp.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StockTransferDTO;
import com.hys.app.model.erp.dto.StockTransferQueryParams;
import com.hys.app.model.erp.dto.StockTransferStatisticsParam;
import com.hys.app.model.erp.vo.StockTransferVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.StockTransferManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 库存调拨API
 *
 * @author 张崧
 * @since 2023-12-12 11:59:06
 */
@RestController
@RequestMapping("/admin/erp/stockTransfer")
@Api(tags = "库存调拨API")
@Validated
public class StockTransferManagerController {

    @Autowired
    private StockTransferManager stockTransferManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<StockTransferVO> list(StockTransferQueryParams queryParams) {
        return stockTransferManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "创建调拨单")
    public void add(@RequestBody @Valid StockTransferDTO stockTransferDTO) {
        stockTransferManager.add(stockTransferDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "编辑ID为[${id}]的调拨单")
    public void edit(@PathVariable Long id, @RequestBody @Valid StockTransferDTO stockTransferDTO) {
        stockTransferDTO.setId(id);
        stockTransferManager.edit(stockTransferDTO);
    }

    @ApiOperation(value = "提交调拨单")
    @PostMapping("/{id}/submit")
    @Log(client = LogClient.admin, detail = "提交ID为[${id}]的调拨单")
    public void submit(@PathVariable Long id) {
        stockTransferManager.submit(id);
    }

    @ApiOperation(value = "撤销提交调拨单")
    @PostMapping("/{id}/withdraw")
    @Log(client = LogClient.admin, detail = "撤销提交ID为[${id}]的调拨单")
    public void withdraw(@PathVariable Long id) {
        stockTransferManager.withdraw(id);
    }

    @ApiOperation(value = "批量确认调拨单")
    @PostMapping("/{ids}/confirm")
    @Log(client = LogClient.admin, detail = "确认ID为[${ids}]的调拨单")
    public void confirm(@PathVariable List<Long> ids, @NotNull(message = "调入人id不能为空") Long handleById) {
        stockTransferManager.confirm(ids, handleById);
    }

    @ApiOperation(value = "批量退回调拨单")
    @PostMapping("/{ids}/reject")
    @Log(client = LogClient.admin, detail = "退回ID为[${ids}]的调拨单")
    public void reject(@PathVariable List<Long> ids, Long handleById) {
        stockTransferManager.reject(ids, handleById);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public StockTransferVO getDetail(@PathVariable Long id) {
        return stockTransferManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的调拨单")
    public void delete(@PathVariable List<Long> ids) {
        stockTransferManager.delete(ids);
    }

    @ApiOperation(value = "查询库存调拨统计分页列表数据")
    @GetMapping("/statistics")
    public WebPage statistics(StockTransferStatisticsParam params) {
        return stockTransferManager.statistics(params);
    }

    @ApiOperation(value = "导出库存调拨统计列表")
    @GetMapping("/export")
    public void export(HttpServletResponse response, StockTransferStatisticsParam params) {
        this.stockTransferManager.export(response, params);
    }
}

