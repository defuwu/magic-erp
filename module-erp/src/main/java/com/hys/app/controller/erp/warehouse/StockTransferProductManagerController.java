package com.hys.app.controller.erp.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.StockTransferProductQueryParams;
import com.hys.app.model.erp.vo.StockTransferProductVO;
import com.hys.app.service.erp.StockTransferProductManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 调拨单商品明细API
 *
 * @author 张崧
 * @since 2023-12-12 16:34:15
 */
@RestController
@RequestMapping("/admin/erp/stockTransferProduct")
@Api(tags = "调拨单商品明细API")
@Validated
public class StockTransferProductManagerController {

    @Autowired
    private StockTransferProductManager stockTransferProductManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<StockTransferProductVO> list(StockTransferProductQueryParams queryParams) {
        return stockTransferProductManager.list(queryParams);
    }

}

