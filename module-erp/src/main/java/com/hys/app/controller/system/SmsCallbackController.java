package com.hys.app.controller.system;

import com.hys.app.service.system.SmsTemplateManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

/**
 * 阿里云短信回调控制器
 *
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-28 16:04:23
 */
@RestController
@RequestMapping("/admin/sms/callback")
@ApiIgnore
public class SmsCallbackController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SmsTemplateManager smsTemplateManager;

    /**
     * 短信模板审核回调接口 暂时支持 smsAliPlugin smsZtPlugin
     *
     * @param type 回调类型
     * @param json 回调body参数
     * @return
     */
    @PostMapping("/template/{type}")
    public Map templateAuditCallback(@PathVariable("type") String type, @RequestBody String json) {
        logger.info("短信模板审核结果回调：" + json);

        Map map = this.smsTemplateManager.templateAuditCallback(json, type);
        return map;
    }

    /**
     * 短信发送结果回调接口
     *
     * @param type 回调类型  暂时支持 smsAliPlugin smsZtPlugin
     * @param json 回调body参数
     * @return
     */
    @PostMapping("/send/{type}")
    public Map smsSendCallback(@PathVariable("type") String type, @RequestBody String json) {
        logger.info("短信发送结果回调：" + json);
        return this.smsTemplateManager.smsSendCallback(json, type);
    }

}