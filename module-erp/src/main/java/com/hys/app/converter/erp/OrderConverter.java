package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.dos.OrderItemDO;
import com.hys.app.model.erp.dos.OrderPaymentDO;
import com.hys.app.model.erp.dos.StoreDO;
import com.hys.app.model.erp.dto.OrderDTO;
import com.hys.app.model.erp.enums.OrderStatusEnum;
import com.hys.app.model.erp.enums.OrderTypeEnum;
import com.hys.app.model.erp.vo.OrderExcelVO;
import com.hys.app.model.erp.vo.OrderItemVO;
import com.hys.app.model.erp.vo.OrderVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.Map;

/**
 * 订单 Converter
 *
 * @author 张崧
 * 2024-01-24 15:58:31
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OrderConverter {

    OrderDO convert(OrderDTO orderDTO);

    @Mapping(target = "allowable", expression = "java(new com.hys.app.model.erp.vo.OrderAllowable(orderDO))")
    @Mapping(target = "statusText", expression = "java(orderDO.getStatus().getText())")
    @Mapping(target = "deliveryTypeText", expression = "java(orderDO.getDeliveryType().getText())")
    @Mapping(target = "paymentStatusText", expression = "java(orderDO.getPaymentStatus().getText())")
    OrderVO convert(OrderDO orderDO);

    List<OrderVO> convertList(List<OrderDO> list);

    List<OrderExcelVO> convertExcel(List<OrderVO> list);

    WebPage<OrderVO> convertPage(WebPage<OrderDO> webPage);

    default OrderDO combination(OrderDTO orderDTO) {
        boolean isToC = orderDTO.getType() == OrderTypeEnum.TO_C;
        OrderDO orderDO = convert(orderDTO);
        if (isToC) {
            // 会员
            orderDO.setMemberName(orderDTO.getMemberDO().getName());
            orderDO.setMemberMobile(orderDTO.getMemberDO().getMobile());
        } else {
            // 企业
            orderDO.setMemberName(orderDTO.getEnterpriseDO().getName());
            orderDO.setMemberMobile(orderDTO.getEnterpriseDO().getMobile());
        }
        // 仓库
        orderDO.setWarehouseName(orderDTO.getWarehouseDO().getName());
        // 部门
        orderDO.setDeptId(orderDTO.getDeptDO().getId());
        orderDO.setDeptName(orderDTO.getDeptDO().getName());
        // 销售经理
        orderDO.setMarketingName(orderDTO.getMarketingManagerDO() == null ? "" : orderDTO.getMarketingManagerDO().getRealName());
        // 配送信息
        orderDO.setStoreName(orderDTO.getStoreDO() == null ? "" : orderDTO.getStoreDO().getStoreName());
        orderDO.setWarehouseOutFlag(false);
        orderDO.setShipFlag(false);

        return orderDO;
    }

    default OrderVO convert(OrderDO orderDO, List<OrderItemDO> itemList, List<OrderPaymentDO> paymentList, StoreDO storeDO, Map<Long, Integer> stockNumMap) {
        OrderVO orderVO = convert(orderDO);

        // 订单明细
        List<OrderItemVO> orderItemList = convertItemList(itemList);
        orderItemList.forEach(orderItemVO -> {
            if (orderDO.getType() == OrderTypeEnum.TO_B) {
                orderItemVO.setStockNum(stockNumMap.get(orderItemVO.getProductId()));
            }else {
                orderItemVO.setStockNum(stockNumMap.get(orderItemVO.getBatchId()));
            }
        });
        orderVO.setItemList(orderItemList);
        // 支付列表
        orderVO.setPaymentList(paymentList);
        // 门店信息
        orderVO.setStore(storeDO);

        return orderVO;
    }

    List<OrderItemVO> convertItemList(List<OrderItemDO> itemList);

    default OrderStatusEnum convertAuditStatus(Boolean isPass, OrderTypeEnum type) {
        if (isPass) {
            return type == OrderTypeEnum.TO_C ? OrderStatusEnum.COMPLETE : OrderStatusEnum.WAIT_WAREHOUSE_OUT;
        } else {
            return OrderStatusEnum.AUDIT_REJECT;
        }
    }
}

