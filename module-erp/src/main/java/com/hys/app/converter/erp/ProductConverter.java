package com.hys.app.converter.erp;

import cn.hutool.core.collection.CollUtil;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.GoodsDO;
import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dto.ProductDTO;
import com.hys.app.model.erp.dto.ProductSpec;
import com.hys.app.model.erp.vo.ProductVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 产品 Convert
 *
 * @author 张崧
 * 2023-11-30 16:06:44
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProductConverter {

    @Mapping(target = "specification", expression = "java(this.formatSpecification(productDTO.getSpecList()))")
    ProductDO convert(ProductDTO productDTO);

    ProductVO convert(ProductDO productDO);

    List<ProductVO> convert(List<ProductDO> webPage);

    List<ProductDO> convertList(List<ProductDTO> list);

    WebPage<ProductVO> convert(WebPage<ProductDO> webPage);

    default WebPage<ProductVO> combination(WebPage<ProductDO> webPage, Map<Long, String> categoryNameMap, Map<Long, Integer> stockNumMap) {
        WebPage<ProductVO> page = convert(webPage);

        // 填充分类名称
        for (ProductVO productVO : page.getData()) {
            productVO.setCategoryName(categoryNameMap.get(productVO.getCategoryId()));
            productVO.setStockNum(stockNumMap.get(productVO.getId()));
        }

        return page;
    }

    default List<ProductDO> convertList(List<ProductDTO> productDTOList, GoodsDO goodsDO) {
        List<ProductDO> productList = convertList(productDTOList);
        productList.forEach(productDO -> productDO.setPropertiesFromGoods(goodsDO));
        return productList;
    }

    default String formatSpecification(List<ProductSpec> specList) {
        if (CollUtil.isEmpty(specList)) {
            return "/";
        }
        return specList.stream().map(ProductSpec::getSpecValue).collect(Collectors.joining(","));
    }
}

