package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.SupplierDO;
import com.hys.app.model.erp.dto.SupplierDTO;
import com.hys.app.model.erp.vo.SupplierVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

/**
 * 供应商 Convert
 *
 * @author 张崧
 * 2023-11-29 14:20:18
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SupplierConverter {

    SupplierDO convert(SupplierDTO supplierDTO);

    SupplierVO convert(SupplierDO supplierDO);

    List<SupplierVO> convert(List<SupplierDO> list);

    WebPage<SupplierVO> convert(WebPage<SupplierDO> webPage);

}

