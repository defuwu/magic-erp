package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.erp.dos.ChangeForm;
import com.hys.app.model.erp.dto.ChangeFormStatisticsQueryParam;
import com.hys.app.model.erp.vo.ChangeFormStatistics;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 换货单Mapper
 * @author dmy
 * 2023-12-05
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface ChangeFormMapper extends BaseMapper<ChangeForm> {

    /**
     * 查询商品换货单商品统计分页列表数据
     *
     * @param page 分页参数
     * @param param 查询参数
     * @return
     */
    IPage<ChangeFormStatistics> selectFormPage(Page page, @Param("param") ChangeFormStatisticsQueryParam param);

    /**
     * 查询换货单商品统计信息集合
     *
     * @param param 查询参数
     * @return
     */
    List<ChangeFormStatistics> selectFormList(@Param("param") ChangeFormStatisticsQueryParam param);
}
