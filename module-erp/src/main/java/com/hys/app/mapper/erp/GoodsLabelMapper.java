package com.hys.app.mapper.erp;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.dos.GoodsLabelDO;
import com.hys.app.model.erp.dto.GoodsLabelQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 商品标签 Mapper
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
@Mapper
public interface GoodsLabelMapper extends BaseMapperX<GoodsLabelDO> {

    default WebPage<GoodsLabelDO> selectPage(GoodsLabelQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(GoodsLabelDO::getName, params.getName())
                .betweenIfPresent(BaseDO::getCreateTime, params.getCreateTime())
                .orderByDesc(GoodsLabelDO::getId)
                .page(params);
    }

}

