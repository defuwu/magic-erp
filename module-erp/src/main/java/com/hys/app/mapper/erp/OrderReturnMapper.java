package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.OrderReturnDO;
import com.hys.app.model.erp.dto.OrderReturnQueryParams;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.OrderReturnStatisticsQueryParam;
import com.hys.app.model.erp.vo.OrderReturnStatistics;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单退货的Mapper
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
public interface OrderReturnMapper extends BaseMapperX<OrderReturnDO> {

    default WebPage<OrderReturnDO> selectPage(OrderReturnQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(OrderReturnDO::getSn, params.getSn())
                .likeIfPresent(OrderReturnDO::getOrderSn, params.getOrderSn())
                .eqIfPresent(OrderReturnDO::getOrderType, params.getOrderType())
                .eqIfPresent(OrderReturnDO::getDeptId, params.getDeptId())
                .eqIfPresent(OrderReturnDO::getWarehouseId, params.getWarehouseId())
                .likeIfPresent(OrderReturnDO::getDistributionName, params.getDistributionName())
                .eqIfPresent(OrderReturnDO::getHandleByName, params.getHandleBy())
                .eqIfPresent(OrderReturnDO::getStatus, params.getStatus())
                .eqIfPresent(OrderReturnDO::getReturnTime, params.getReturnTime())
                .geIfPresent(OrderReturnDO::getReturnTime, params.getStartTime())
                .leIfPresent(OrderReturnDO::getReturnTime, params.getEndTime())
                .orderByDesc(OrderReturnDO::getReturnTime)
                .page(params);
    }

    /**
     * 查询订单退货统计分页列表数据
     *
     * @param page 分页参数
     * @param param 查询参数
     * @return
     */
    IPage<OrderReturnStatistics> selectOrderReturnPage(Page page, @Param("param") OrderReturnStatisticsQueryParam param);

    /**
     * 导出订单退货统计列表
     *
     * @param param 查询参数
     * @return
     */
    List<OrderReturnStatistics> selectOrderReturnList(@Param("param") OrderReturnStatisticsQueryParam param);
}

