package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.hys.app.model.erp.dto.WarehouseEntryQueryParams;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.WarehouseEntryStatisticsParam;
import com.hys.app.model.erp.vo.WarehouseEntryStatistics;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 入库单的Mapper
 *
 * @author 张崧
 * @since 2023-12-05 11:25:07
 */
public interface WarehouseEntryMapper extends BaseMapperX<WarehouseEntryDO> {

    default WebPage<WarehouseEntryDO> selectPage(WarehouseEntryQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(WarehouseEntryDO::getSn, params.getSn())
                .geIfPresent(WarehouseEntryDO::getEntryTime, params.getEntryTimeBegin())
                .leIfPresent(WarehouseEntryDO::getEntryTime, params.getEntryTimeEnd())
                .eqIfPresent(WarehouseEntryDO::getDeptId, params.getDeptId())
                .eqIfPresent(WarehouseEntryDO::getWarehouseId, params.getWarehouseId())
                .eqIfPresent(WarehouseEntryDO::getSupplierId, params.getSupplierId())
                .eqIfPresent(WarehouseEntryDO::getContractId, params.getContractId())
                .likeIfPresent(WarehouseEntryDO::getHandledByName, params.getHandledBy())
                .likeIfPresent(WarehouseEntryDO::getPlateNumber, params.getPlateNumber())
                .eqIfPresent(WarehouseEntryDO::getPurchasePlanId, params.getPurchasePlanId())
                .likeIfPresent(WarehouseEntryDO::getDeliveryNumber, params.getDeliveryNumber())
                .likeIfPresent(WarehouseEntryDO::getContractAttachment, params.getContractAttachment())
                .eqIfPresent(WarehouseEntryDO::getStatus, params.getStatus())
                .eqIfPresent(WarehouseEntryDO::getSupplierSettlementFlag, params.getSupplierSettlementFlag())
                .orderByDesc(WarehouseEntryDO::getEntryTime)
                .page(params);
    }

    /**
     * 查询入库单统计分页列表数据
     *
     * @param page 分页参数
     * @param param 查询参数
     * @return
     */
    IPage<WarehouseEntryStatistics> selectWarehouseEntryPage(Page page, @Param("param") WarehouseEntryStatisticsParam param);

    /**
     * 查询导出入库单统计列表
     *
     * @param param 查询参数
     * @return
     */
    List<WarehouseEntryStatistics> selectWarehouseEntryList(@Param("param") WarehouseEntryStatisticsParam param);
}

