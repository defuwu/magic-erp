package com.hys.app.mapper.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hys.app.model.system.dto.DictDataQueryParams;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.DictDataDO;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.database.mybatisplus.wrapper.LambdaQueryWrapperX;
import org.apache.ibatis.annotations.Mapper;

import java.util.Collection;
import java.util.List;

@Mapper
public interface DictDataMapper extends BaseMapperX<DictDataDO> {

    default DictDataDO selectByDictTypeAndValue(String dictType, String value) {
        return selectOne(DictDataDO::getDictType, dictType, DictDataDO::getValue, value);
    }

    default DictDataDO selectByDictTypeAndLabel(String dictType, String label) {
        return selectOne(DictDataDO::getDictType, dictType, DictDataDO::getLabel, label);
    }

    default List<DictDataDO> selectByDictTypeAndValues(String dictType, Collection<String> values) {
        return selectList(new LambdaQueryWrapper<DictDataDO>().eq(DictDataDO::getDictType, dictType)
                .in(DictDataDO::getValue, values));
    }

    default long selectCountByDictType(String dictType) {
        return selectCount(DictDataDO::getDictType, dictType);
    }

    default WebPage<DictDataDO> selectPage(DictDataQueryParams reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<DictDataDO>()
                .likeIfPresent(DictDataDO::getLabel, reqVO.getLabel())
                .eqIfPresent(DictDataDO::getDictType, reqVO.getDictType())
                .orderByAsc(DictDataDO::getDictType, DictDataDO::getSort));
    }

    default List<DictDataDO> selectListByTypeAndStatus(String dictType) {
        return selectList(new LambdaQueryWrapper<DictDataDO>()
                .eq(DictDataDO::getDictType, dictType));
    }

    default void updateToNotDefault(String dictType){
        lambdaUpdate()
                .set(DictDataDO::getDefaultFlag, false)
                .eq(DictDataDO::getDictType, dictType)
                .update();
    }

    default List<DictDataDO> selectListByDictType(String dictType){
        return lambdaQuery()
                .eq(DictDataDO::getDictType, dictType)
                .list();
    }
}
