package com.hys.app.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.system.dos.SystemLogs;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 系统日志Mapper
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:58
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface SystemLogsMapper extends BaseMapper<SystemLogs> {


}