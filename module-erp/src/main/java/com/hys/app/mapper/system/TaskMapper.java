package com.hys.app.mapper.system;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.system.dos.TaskDO;
import com.hys.app.model.system.dto.TaskQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 任务 Mapper
 *
 * @author 张崧
 * 2024-01-23 10:43:35
 */
@Mapper
public interface TaskMapper extends BaseMapperX<TaskDO> {

    default WebPage<TaskDO> selectPage(TaskQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(TaskDO::getName, params.getName())
                .eqIfPresent(TaskDO::getType, params.getType())
                .eqIfPresent(TaskDO::getSubType, params.getSubType())
                .eqIfPresent(TaskDO::getParams, params.getParams())
                .eqIfPresent(TaskDO::getStatus, params.getStatus())
                .eqIfPresent(TaskDO::getResult, params.getResult())
                .eqIfPresent(TaskDO::getThreadId, params.getThreadId())
                .betweenIfPresent(BaseDO::getCreateTime, params.getCreateTime())
                .orderByDesc(TaskDO::getId)
                .page(params);
    }

}

