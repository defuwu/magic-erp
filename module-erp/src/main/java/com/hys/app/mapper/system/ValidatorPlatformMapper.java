package com.hys.app.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.system.dos.ValidatorPlatformDO;
import org.apache.ibatis.annotations.CacheNamespace;


/**
 * 验证平台相关的Mapper
 * @author zhanghao
 * @version v1.0
 * @since v7.2.2
 * 2020/7/21
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface ValidatorPlatformMapper extends BaseMapper<ValidatorPlatformDO> {
}
