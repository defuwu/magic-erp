package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.enums.FinanceAmountTypeEnum;
import com.hys.app.model.erp.enums.FinanceExpandTypeEnum;
import com.hys.app.model.erp.enums.FinanceIncomeTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 财务明细 DO
 *
 * @author 张崧
 * 2024-03-15 17:40:23
 */
@TableName("erp_finance_item")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FinanceItemDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "sn", value = "财务明细编号")
    private String sn;

    /**
     * 收入 {@link FinanceIncomeTypeEnum}
     * 支出 {@link FinanceExpandTypeEnum}
     */
    @ApiModelProperty(name = "source_type", value = "来源")
    private String sourceType;

    @ApiModelProperty(name = "source_sn", value = "来源编号")
    private String sourceSn;

    @ApiModelProperty(name = "amount", value = "金额")
    private Double amount;

    @ApiModelProperty(name = "amount_type", value = "收入/支出")
    private FinanceAmountTypeEnum amountType;

}
