package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 采购合同产品实体
 * @Author dmy
 * 2023-12-05
 */
@TableName("es_procurement_contract_product")
@ApiModel
public class ProcurementContractProduct implements Serializable {

    private static final long serialVersionUID = -7488218981149467236L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 合同ID
     */
    @ApiModelProperty(name = "contract_id", value = "合同ID")
    private Long contractId;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格")
    private String specification;
    /**
     * 分类id
     */
    @ApiModelProperty(name = "category_id", value = "分类id")
    private Long categoryId;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位")
    private String unit;
    /**
     * 条形码
     */
    @ApiModelProperty(name = "barcode", value = "条形码")
    private String barcode;
    /**
     * 单价
     */
    @ApiModelProperty(name = "price", value = "单价")
    private Double price;
    /**
     * 数量
     */
    @ApiModelProperty(name = "num", value = "数量")
    private Integer num;
    /**
     * 税率
     */
    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;
    /**
     * 合价
     */
    @ApiModelProperty(name = "total_price", value = "合价")
    private Double totalPrice;
    /**
     * 入库数量
     */
    @ApiModelProperty(name = "stock_num", value = "入库数量")
    private Integer stockNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProcurementContractProduct that = (ProcurementContractProduct) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(contractId, that.contractId) &&
                Objects.equals(goodsId, that.goodsId) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productSn, that.productSn) &&
                Objects.equals(specification, that.specification) &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(categoryName, that.categoryName) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(barcode, that.barcode) &&
                Objects.equals(price, that.price) &&
                Objects.equals(num, that.num) &&
                Objects.equals(taxRate, that.taxRate) &&
                Objects.equals(totalPrice, that.totalPrice) &&
                Objects.equals(stockNum, that.stockNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, contractId, goodsId, productId, productName, productSn, specification, categoryId, categoryName, unit, barcode, price, num, taxRate, totalPrice, stockNum);
    }

    @Override
    public String toString() {
        return "ProcurementContractProduct{" +
                "id=" + id +
                ", contractId=" + contractId +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", barcode='" + barcode + '\'' +
                ", price=" + price +
                ", num=" + num +
                ", taxRate=" + taxRate +
                ", totalPrice=" + totalPrice +
                ", stockNum=" + stockNum +
                '}';
    }
}
