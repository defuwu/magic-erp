package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 采购计划实体
 * @author dmy
 * 2023-12-05
 */
@TableName("es_procurement_plan")
@ApiModel
public class ProcurementPlan implements Serializable {

    private static final long serialVersionUID = -8506705760350736364L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 编号
     */
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;
    /**
     * 编制人员ID
     */
    @ApiModelProperty(name = "formation_person_id", value = "编制人员ID")
    private Long formationPersonId;
    /**
     * 编制人员
     */
    @ApiModelProperty(name = "formation_person", value = "编制人员")
    private String formationPerson;
    /**
     * 编制时间
     */
    @ApiModelProperty(name = "formation_time", value = "编制时间")
    private Long formationTime;
    /**
     * 采购说明
     */
    @ApiModelProperty(name = "procurement_desc", value = "采购说明")
    private String procurementDesc;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;
    /**
     * 供应商ID
     */
    @ApiModelProperty(name = "supplier_id", value = "供应商ID")
    private Long supplierId;
    /**
     * 供应商名称
     */
    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;
    /**
     * 计划供应时间
     */
    @ApiModelProperty(name = "supplier_time", value = "计划供应时间")
    private Long supplierTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getFormationPersonId() {
        return formationPersonId;
    }

    public void setFormationPersonId(Long formationPersonId) {
        this.formationPersonId = formationPersonId;
    }

    public String getFormationPerson() {
        return formationPerson;
    }

    public void setFormationPerson(String formationPerson) {
        this.formationPerson = formationPerson;
    }

    public Long getFormationTime() {
        return formationTime;
    }

    public void setFormationTime(Long formationTime) {
        this.formationTime = formationTime;
    }

    public String getProcurementDesc() {
        return procurementDesc;
    }

    public void setProcurementDesc(String procurementDesc) {
        this.procurementDesc = procurementDesc;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Long getSupplierTime() {
        return supplierTime;
    }

    public void setSupplierTime(Long supplierTime) {
        this.supplierTime = supplierTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProcurementPlan plan = (ProcurementPlan) o;
        return Objects.equals(id, plan.id) &&
                Objects.equals(sn, plan.sn) &&
                Objects.equals(deptId, plan.deptId) &&
                Objects.equals(deptName, plan.deptName) &&
                Objects.equals(formationPersonId, plan.formationPersonId) &&
                Objects.equals(formationPerson, plan.formationPerson) &&
                Objects.equals(formationTime, plan.formationTime) &&
                Objects.equals(procurementDesc, plan.procurementDesc) &&
                Objects.equals(createTime, plan.createTime) &&
                Objects.equals(supplierId, plan.supplierId) &&
                Objects.equals(supplierName, plan.supplierName) &&
                Objects.equals(supplierTime, plan.supplierTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sn, deptId, deptName, formationPersonId, formationPerson, formationTime, procurementDesc, createTime, supplierId, supplierName, supplierTime);
    }

    @Override
    public String toString() {
        return "ProcurementPlan{" +
                "id=" + id +
                ", sn='" + sn + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", formationPersonId=" + formationPersonId +
                ", formationPerson='" + formationPerson + '\'' +
                ", formationTime=" + formationTime +
                ", procurementDesc='" + procurementDesc + '\'' +
                ", createTime=" + createTime +
                ", supplierId=" + supplierId +
                ", supplierName='" + supplierName + '\'' +
                ", supplierTime=" + supplierTime +
                '}';
    }
}
