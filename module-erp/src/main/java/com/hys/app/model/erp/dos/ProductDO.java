package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.dto.ProductSpec;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;


/**
 * 产品实体类
 *
 * @author 张崧
 * 2023-11-30 16:06:44
 */
@TableName(value = "erp_product", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class ProductDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    /**
     * 关联 {@link GoodsDO#getId()}
     */
    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;

    // ============= product自己的字段 =============

    @ApiModelProperty(name = "sn", value = "产品编号")
    private String sn;

    @ApiModelProperty(name = "price", value = "销售价格")
    private Double price;

    @ApiModelProperty(name = "cost_price", value = "成本价格")
    private Double costPrice;

    @ApiModelProperty(name = "retail_price", value = "零售价格")
    private Double retailPrice;

    @ApiModelProperty(name = "mkt_price", value = "市场价格")
    private Double mktPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;

    @ApiModelProperty(name = "weight", value = "重量")
    private Double weight;

    @ApiModelProperty(name = "unit", value = "单位")
    private String unit;

    @ApiModelProperty(name = "warning_value", value = "库存预警数")
    private Integer warningValue;

    @ApiModelProperty(name = "specification", value = "规格展示信息")
    private String specification;

    @ApiModelProperty(name = "spec_list", value = "规格列表")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private SpecList specList;

    @ApiModelProperty(name = "image", value = "产品图片")
    private String image;

    // ============= 冗余GoodsDO的字段 =============

    @ApiModelProperty(name = "name", value = "产品名称")
    private String name;

    @ApiModelProperty(name = "category_id", value = "分类id")
    private Long categoryId;

    @ApiModelProperty(name = "brand_id", value = "品牌id")
    private Long brandId;

    @ApiModelProperty(name = "market_enable", value = "是否上架 true上架")
    private Boolean marketEnable;

    public void setPropertiesFromGoods(GoodsDO goodsDO) {
        this.setGoodsId(goodsDO.getId());

        // 冗余GoodsDO的字段
        this.setName(goodsDO.getName());
        this.setCategoryId(goodsDO.getCategoryId());
        this.setBrandId(goodsDO.getBrandId());
        this.setMarketEnable(goodsDO.getMarketEnable());
        this.setImage(goodsDO.getImage());

        // 如果是无规格商品
        if (!goodsDO.getHaveSpec()) {
            // 编号使用商品编号
            this.setSn(goodsDO.getSn());
            // 规格列表置空，防止前端传参错误
            this.setSpecList(new SpecList());
        }
    }

    /**
     * 解决JacksonTypeHandler反序列化问题
     */
    public static class SpecList extends ArrayList<ProductSpec> {
    }
}
