package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;


/**
 * 供应商结算单实体类
 *
 * @author 张崧
 * @since 2023-12-15 14:09:09
 */
@TableName("erp_supplier_settlement")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierSettlementDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "结算单编号")
    private String sn;
    
    @ApiModelProperty(name = "supplier_id", value = "供应商id")
    private Long supplierId;

    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;
    
    @ApiModelProperty(name = "start_time", value = "结算开始时间")
    private Long startTime;
    
    @ApiModelProperty(name = "end_time", value = "结算结束时间")
    private Long endTime;
    
    @ApiModelProperty(name = "total_price", value = "合计金额")
    private Double totalPrice;

}
