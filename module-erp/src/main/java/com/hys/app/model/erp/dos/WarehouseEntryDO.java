package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.enums.WarehouseEntryStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 入库单实体类
 *
 * @author 张崧
 * @since 2023-12-05 11:25:07
 */
@TableName("erp_warehouse_entry")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "入库单编号")
    private String sn;
    
    @ApiModelProperty(name = "entry_time", value = "入库时间")
    private Long entryTime;
    
    @ApiModelProperty(name = "supplier_id", value = "供应商id")
    private Long supplierId;

    @ApiModelProperty(name = "supplier_sn", value = "供应商编号")
    private String supplierSn;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;
    
    @ApiModelProperty(name = "contract_id", value = "合同id")
    private Long contractId;

    @ApiModelProperty(name = "handled_by_id", value = "经手人id")
    private Long handledById;

    @ApiModelProperty(name = "handled_by_name", value = "经手人名称")
    private String handledByName;
    
    @ApiModelProperty(name = "plate_number", value = "车牌号")
    private String plateNumber;
    
    @ApiModelProperty(name = "purchase_plan_id", value = "采购计划id")
    private Long purchasePlanId;
    
    @ApiModelProperty(name = "delivery_number", value = "送货单号")
    private String deliveryNumber;
    
    @ApiModelProperty(name = "contract_attachment", value = "合同附件")
    private String contractAttachment;

    @ApiModelProperty(name = "audit_by", value = "审核人")
    private String auditBy;

    @ApiModelProperty(name = "audit_remark", value = "审核备注")
    private String auditRemark;

    @ApiModelProperty(name = "procurement_plan_sn", value = "采购计划编号")
    private String procurementPlanSn;

    @ApiModelProperty(name = "contract_sn", value = "合同编号")
    private String contractSn;

    @ApiModelProperty(name = "status", value = "入库单状态")
    private WarehouseEntryStatusEnum status;

    @ApiModelProperty(name = "supplier_settlement_flag", value = "是否已生成供应商结算单")
    private Boolean supplierSettlementFlag;

    @ApiModelProperty(name = "supplier_settlement_id", value = "供应商结算单id")
    private Long supplierSettlementId;

}
