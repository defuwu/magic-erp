package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 换货单实体DTO
 *
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ChangeFormDTO implements Serializable {

    private static final long serialVersionUID = 8623988728396956693L;

    /**
     * 订单编号
     */
    @ApiModelProperty(name = "order_sn", value = "订单编号", required = true)
    @NotEmpty(message = "订单编号不能为空")
    private String orderSn;
    /**
     * 服务专员名称
     */
    @ApiModelProperty(name = "staff_name", value = "服务专员名称", required = true)
    @NotEmpty(message = "服务专员名称不能为空")
    private String staffName;
    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID", required = true)
    @NotNull(message = "部门ID不能为空")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称", required = true)
    @NotEmpty(message = "部门名称不能为空")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID", required = true)
    @NotNull(message = "仓库ID不能为空")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称", required = true)
    @NotEmpty(message = "仓库名称不能为空")
    private String warehouseName;
    /**
     * 换货金额
     */
    @ApiModelProperty(name = "change_amount", value = "换货金额", required = true)
    @NotNull(message = "换货金额不能为空")
    private Double changeAmount;
    /**
     * 经手人ID
     */
    @ApiModelProperty(name = "handled_by_id", value = "经手人ID", required = true)
    @NotNull(message = "经手人ID不能为空")
    private Long handledById;
    /**
     * 经手人
     */
    @ApiModelProperty(name = "handled_by", value = "经手人", required = true)
    @NotEmpty(message = "经手人不能为空")
    private String handledBy;
    /**
     * 换货说明
     */
    @ApiModelProperty(name = "change_desc", value = "换货说明", required = true)
    @NotEmpty(message = "换货说明不能为空")
    private String changeDesc;
    /**
     * 换货时间
     */
    @ApiModelProperty(name = "change_time", value = "换货时间", required = true)
    @NotNull(message = "换货时间不能为空")
    private Long changeTime;
    /**
     * 订单退货产品集合
     */
    @ApiModelProperty(name = "return_list", value = "订单退货产品集合", required = true)
    @NotNull(message = "订单退货产品集合不能为空")
    @Valid
    private List<ChangeFormProductDTO> returnList;
    /**
     * 换货产品集合
     */
    @ApiModelProperty(name = "change_list", value = "换货产品集合", required = true)
    @NotNull(message = "换货产品集合不能为空")
    @Valid
    private List<ChangeFormProductDTO> changeList;

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Double getChangeAmount() {
        return changeAmount;
    }

    public void setChangeAmount(Double changeAmount) {
        this.changeAmount = changeAmount;
    }

    public Long getHandledById() {
        return handledById;
    }

    public void setHandledById(Long handledById) {
        this.handledById = handledById;
    }

    public String getHandledBy() {
        return handledBy;
    }

    public void setHandledBy(String handledBy) {
        this.handledBy = handledBy;
    }

    public String getChangeDesc() {
        return changeDesc;
    }

    public void setChangeDesc(String changeDesc) {
        this.changeDesc = changeDesc;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    public List<ChangeFormProductDTO> getReturnList() {
        return returnList;
    }

    public void setReturnList(List<ChangeFormProductDTO> returnList) {
        this.returnList = returnList;
    }

    public List<ChangeFormProductDTO> getChangeList() {
        return changeList;
    }

    public void setChangeList(List<ChangeFormProductDTO> changeList) {
        this.changeList = changeList;
    }

    @Override
    public String toString() {
        return "ChangeFormDTO{" +
                "orderSn='" + orderSn + '\'' +
                ", staffName='" + staffName + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", changeAmount=" + changeAmount +
                ", handledById=" + handledById +
                ", handledBy='" + handledBy + '\'' +
                ", changeDesc='" + changeDesc + '\'' +
                ", changeTime=" + changeTime +
                ", returnList=" + returnList +
                ", changeList=" + changeList +
                '}';
    }
}
