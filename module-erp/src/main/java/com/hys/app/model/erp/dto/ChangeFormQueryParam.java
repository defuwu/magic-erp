package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 换货单查询参数实体
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ChangeFormQueryParam extends BaseQueryParam implements Serializable {

    private static final long serialVersionUID = 935059625596467142L;

    @ApiModelProperty(name = "sn", value = "换货单编号")
    private String sn;

    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;

    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间")
    private Long endTime;

    @ApiModelProperty(name = "staff_name", value = "服务专员名称")
    private String staffName;

    @ApiModelProperty(name = "status", value = "状态", allowableValues = "WAIT,PASS,REJECT,CONFIRMED")
    private String status;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ChangeFormQueryParam{" +
                "sn='" + sn + '\'' +
                ", deptId=" + deptId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", staffName='" + staffName + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
