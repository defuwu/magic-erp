package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 企业 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-03-25 15:44:10
 */
@Data
@ApiModel(value = "企业 新增|编辑 请求参数")
public class EnterpriseDTO {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "企业名称")
    @NotBlank(message = "企业名称不能为空")
    private String name;
    
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    
    @ApiModelProperty(name = "mobile", value = "手机号")
    private String mobile;
    
    @ApiModelProperty(name = "email", value = "邮箱")
    private String email;
    
    @ApiModelProperty(name = "zip_code", value = "邮编")
    private String zipCode;
    
    @ApiModelProperty(name = "tax_num", value = "纳税人识别号")
    private String taxNum;
    
    @ApiModelProperty(name = "bank_name", value = "开户银行")
    private String bankName;
    
    @ApiModelProperty(name = "address", value = "地址")
    private String address;
    
    @ApiModelProperty(name = "linkman", value = "联系人")
    private String linkman;
    
    @ApiModelProperty(name = "link_phone", value = "联系电话")
    private String linkPhone;
    
    @ApiModelProperty(name = "fax", value = "传真")
    private String fax;
    
    @ApiModelProperty(name = "bank_account", value = "银行账号")
    private String bankAccount;
    
    @ApiModelProperty(name = "bank_account_name", value = "银行户名")
    private String bankAccountName;
    
    @ApiModelProperty(name = "region_ids", value = "地区id集合")
    private String regionIds;
    
    @ApiModelProperty(name = "region_names", value = "地区名称集合")
    private String regionNames;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
}

