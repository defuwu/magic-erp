package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品标签-商品关联表 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
@Data
@ApiModel(value = "商品标签-商品关联表 新增|编辑 请求参数")
public class GoodsLabelRelationDTO {

    @ApiModelProperty(name = "label_id", value = "标签id")
    @NotNull(message = "标签id不能为空")
    private Long labelId;

    @ApiModelProperty(name = "goods_ids", value = "商品id集合")
    @NotNull(message = "商品不能为空")
    private List<Long> goodsIds;

}

