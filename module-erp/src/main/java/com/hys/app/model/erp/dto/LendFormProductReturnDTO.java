package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 商品借出单商品归还实体DTO
 *
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class LendFormProductReturnDTO implements Serializable {

    private static final long serialVersionUID = 4848134291431643665L;

    /**
     * 借出单商品主键ID
     */
    @ApiModelProperty(name = "id", value = "借出单商品主键ID", required = true)
    @NotNull(message = "借出单商品主键ID不能为空")
    private Long id;
    /**
     * 归还数量
     */
    @ApiModelProperty(name = "return_num", value = "归还数量", required = true)
    @NotNull(message = "归还数量不能为空")
    private Integer returnNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getReturnNum() {
        return returnNum;
    }

    public void setReturnNum(Integer returnNum) {
        this.returnNum = returnNum;
    }

    @Override
    public String toString() {
        return "LendFormProductReturnDTO{" +
                "id=" + id +
                ", returnNum=" + returnNum +
                '}';
    }
}
