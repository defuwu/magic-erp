package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hys.app.model.erp.dos.*;
import com.hys.app.model.erp.enums.OrderPaymentStatusEnum;
import com.hys.app.model.erp.enums.OrderTypeEnum;
import com.hys.app.model.goods.dos.BrandDO;
import com.hys.app.model.goods.dos.CategoryDO;
import com.hys.app.model.system.dos.DeptDO;
import com.hys.app.model.erp.enums.OrderDeliveryType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 订单 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-01-24 15:58:31
 */
@Data
@ApiModel(value = "订单 新增|编辑 请求参数")
public class OrderDTO {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "type", value = "订单类型")
    @NotNull(message = "订单类型不能为空")
    private OrderTypeEnum type;

    @ApiModelProperty(name = "member_id", value = "会员id/企业id")
    @NotNull(message = "会员id/企业id不能为空")
    private Long memberId;

    @ApiModelProperty(name = "order_time", value = "下单时间")
    @NotNull(message = "下单时间不能为空")
    private Long orderTime;

    @ApiModelProperty(name = "payment_status", value = "支付状态")
    @NotNull(message = "支付状态不能为空")
    private OrderPaymentStatusEnum paymentStatus;

    @ApiModelProperty(name = "payment_time", value = "支付时间")
    private Long paymentTime;

    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    @NotNull(message = "仓库id不能为空")
    private Long warehouseId;

    @ApiModelProperty(name = "marketing_id", value = "销售经理id")
    private Long marketingId;

    @ApiModelProperty(name = "attachment", value = "附件")
    private String attachment;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

    @ApiModelProperty(name = "delivery_type", value = "配送方式")
    private OrderDeliveryType deliveryType;

    @ApiModelProperty(name = "store_id", value = "自提门店id")
    private Long storeId;

    @ApiModelProperty(name = "item_list", value = "订单项列表")
    @NotEmpty(message = "订单项不能为空")
    @Valid
    private List<OrderItemDTO> itemList;

    @ApiModelProperty(name = "payment_list", value = "支付列表")
    @NotEmpty(message = "支付信息不能为空")
    @Valid
    private List<OrderPaymentDTO> paymentList;

    @ApiModelProperty(name = "total_price", value = "销售金额")
    @NotNull(message = "销售金额不能为空")
    @Min(value = 0, message = "销售金额不能小于0")
    private Double totalPrice;

    @ApiModelProperty(name = "discount_price", value = "优惠金额")
    @NotNull(message = "优惠金额不能为空")
    @Min(value = 0, message = "优惠金额不能小于0")
    private Double discountPrice;

    @ApiModelProperty(name = "tax_price", value = "税额")
    @NotNull(message = "税额不能为空")
    @Min(value = 0, message = "税额不能小于0")
    private Double taxPrice;

    @ApiModelProperty(name = "pay_price", value = "应付金额")
    @NotNull(message = "应付金额不能为空")
    @Min(value = 0, message = "应付金额不能小于0")
    private Double payPrice;

    @ApiModelProperty(name = "deposit_price", value = "定金金额")
    @NotNull(message = "定金金额不能为空")
    @Min(value = 0, message = "定金金额不能小于0")
    private Double depositPrice;

    // ============ 校验相关 ============

    @JsonIgnore
    @AssertFalse(message = "门店id不能为空")
    public boolean isCheckStore() {
        return deliveryType != null && deliveryType == OrderDeliveryType.self_pick && storeId == null;
    }

    @JsonIgnore
    @AssertFalse(message = "定金金额不能大于应付金额")
    public boolean isCheckDepositPrice() {
        return depositPrice > payPrice;
    }

    @JsonIgnore
    @AssertFalse(message = "支付时间不能为空")
    public boolean isCheckPaymentTime() {
        return paymentStatus == OrderPaymentStatusEnum.PAY && paymentTime == null;
    }

    @JsonIgnore
    @AssertFalse(message = "配送方式不能为空")
    public boolean isCheckDelivery() {
        return type == OrderTypeEnum.TO_B && deliveryType == null;
    }


    // ============ 非前端传参 ============

    @JsonIgnore
    @ApiModelProperty(value = "会员", hidden = true)
    private MemberDO memberDO;

    @JsonIgnore
    @ApiModelProperty(value = "企业", hidden = true)
    private EnterpriseDO enterpriseDO;

    @JsonIgnore
    @ApiModelProperty(value = "仓库", hidden = true)
    private WarehouseDO warehouseDO;

    @JsonIgnore
    @ApiModelProperty(value = "部门", hidden = true)
    private DeptDO deptDO;

    @JsonIgnore
    @ApiModelProperty(value = "销售经理", hidden = true)
    private MarketingManagerDO marketingManagerDO;

    @JsonIgnore
    @ApiModelProperty(value = "自提门店", hidden = true)
    private StoreDO storeDO;

    @JsonIgnore
    @ApiModelProperty(value = "产品", hidden = true)
    private Map<Long, ProductDO> productMap;

    @JsonIgnore
    @ApiModelProperty(value = "库存批次", hidden = true)
    private Map<Long, WarehouseEntryBatchDO> batchMap;

    @JsonIgnore
    @ApiModelProperty(value = "收款账户", hidden = true)
    private Map<Long, CollectingAccountDO> collectingAccountMap;

    @JsonIgnore
    @ApiModelProperty(value = "商品分类", hidden = true)
    private Map<Long, CategoryDO> categoryMap;

    @JsonIgnore
    @ApiModelProperty(value = "品牌", hidden = true)
    private Map<Long, BrandDO> brandMap;

}

