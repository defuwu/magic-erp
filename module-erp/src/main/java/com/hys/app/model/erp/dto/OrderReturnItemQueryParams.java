package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 订单退货项查询参数
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OrderReturnItemQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "order_return_id", value = "订单退货id")
    private Long orderReturnId;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单号")
    private Long warehouseEntrySn;
    
    @ApiModelProperty(name = "warehouse_entry_batch_id", value = "入库批次id")
    private Long warehouseEntryBatchId;
    
    @ApiModelProperty(name = "warehouse_entry_batch_sn", value = "入库批次号")
    private String warehouseEntryBatchSn;
    
    @ApiModelProperty(name = "product_id", value = "商品id")
    private Long productId;
    
    @ApiModelProperty(name = "product_sn", value = "商品编号")
    private String productSn;
    
    @ApiModelProperty(name = "product_name", value = "商品名称")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "商品规格")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_unit", value = "商品单位")
    private String productUnit;
    
    @ApiModelProperty(name = "product_price", value = "商品单价")
    private Double productPrice;
    
    @ApiModelProperty(name = "order_num", value = "订单的购买数量")
    private Integer orderNum;
    
    @ApiModelProperty(name = "return_num", value = "退货数量")
    private Integer returnNum;
    
}

