package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 库存报损单统计查询参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class StockDamageReportStatisticsParam extends StockDamageReportParam implements Serializable {

    private static final long serialVersionUID = 9139963437002379776L;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(name = "damage_type", value = "报损类型")
    private String damageType;

    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getDamageType() {
        return damageType;
    }

    public void setDamageType(String damageType) {
        this.damageType = damageType;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    @Override
    public String toString() {
        return "StockDamageReportStatisticsParam{" +
                "productName='" + productName + '\'' +
                ", categoryId=" + categoryId +
                ", damageType='" + damageType + '\'' +
                ", productSn='" + productSn + '\'' +
                '}';
    }
}
