package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 库存盘点单查询参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class StockInventoryParam extends BaseQueryParam implements Serializable {

    private static final long serialVersionUID = 8262063047050222023L;

    @ApiModelProperty(name = "sn", value = "库存盘点单编号")
    private String sn;

    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;

    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间")
    private Long endTime;

    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;

    @ApiModelProperty(name = "status", value = "状态", allowableValues = "NEW,WAIT,PASS,REJECT")
    private String status;

    @ApiModelProperty(name = "inventory_person", value = "盘点人")
    private String inventoryPerson;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInventoryPerson() {
        return inventoryPerson;
    }

    public void setInventoryPerson(String inventoryPerson) {
        this.inventoryPerson = inventoryPerson;
    }

    @Override
    public String toString() {
        return "StockInventoryParam{" +
                "sn='" + sn + '\'' +
                ", deptId=" + deptId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", warehouseId=" + warehouseId +
                ", status='" + status + '\'' +
                ", inventoryPerson='" + inventoryPerson + '\'' +
                '}';
    }
}
