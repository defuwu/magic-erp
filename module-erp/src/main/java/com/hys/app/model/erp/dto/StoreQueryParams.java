package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 门店查询参数
 *
 * @author 张崧
 * @since 2023-12-11 17:47:09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class StoreQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "store_name", value = "门店名称")
    private String storeName;
    
    @ApiModelProperty(name = "address", value = "门店地址")
    private String address;
    
    @ApiModelProperty(name = "contact_person", value = "联系人")
    private String contactPerson;
    
    @ApiModelProperty(name = "telephone", value = "门店联系电话")
    private String telephone;
    
    @ApiModelProperty(name = "create_time", value = "门店创建时间")
    private Long createTime;
    
}

