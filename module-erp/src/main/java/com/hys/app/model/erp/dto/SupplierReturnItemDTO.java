package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 供应商退货项新增/编辑DTO
 *
 * @author 张崧
 * @since 2023-12-14 11:12:46
 */
@Data
public class SupplierReturnItemDTO {

    @ApiModelProperty(name = "warehouse_entry_batch_id", value = "批次id")
    @NotNull(message = "批次id不能为空")
    private Long warehouseEntryBatchId;

    @ApiModelProperty(name = "return_num", value = "退货数量")
    @NotNull(message = "退货数量不能为空")
    private Integer returnNum;

}

