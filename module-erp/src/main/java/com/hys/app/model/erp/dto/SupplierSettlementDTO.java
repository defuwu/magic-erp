package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 供应商结算单新增/编辑DTO
 *
 * @author 张崧
 * @since 2023-12-15 14:09:09
 */
@Data
public class SupplierSettlementDTO {

    @ApiModelProperty(name = "warehouse_entry_ids", value = "入库单id集合")
    @NotEmpty(message = "入库单id不能为空")
    private List<Long> warehouseEntryIds;

    @JsonIgnore
    private List<WarehouseEntryDO> warehouseEntryList;
}

