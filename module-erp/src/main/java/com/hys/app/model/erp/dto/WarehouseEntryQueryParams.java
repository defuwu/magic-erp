package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 入库单查询参数
 *
 * @author 张崧
 * @since 2023-12-05 11:25:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "入库单编号")
    private String sn;
    
    @ApiModelProperty(name = "entry_time", value = "入库时间")
    private Long entryTime;
    
    @ApiModelProperty(name = "supplier_id", value = "供应商id")
    private Long supplierId;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "contract_id", value = "合同id")
    private Long contractId;
    
    @ApiModelProperty(name = "handled_by", value = "经手人")
    private String handledBy;
    
    @ApiModelProperty(name = "plate_number", value = "车牌号")
    private String plateNumber;
    
    @ApiModelProperty(name = "purchase_plan_id", value = "采购计划id")
    private Long purchasePlanId;
    
    @ApiModelProperty(name = "delivery_number", value = "送货单号")
    private String deliveryNumber;
    
    @ApiModelProperty(name = "contract_attachment", value = "合同附件")
    private String contractAttachment;
    
    @ApiModelProperty(name = "status", value = "入库单状态")
    private String status;

    @ApiModelProperty(name = "entry_time_begin", value = "入库时间开始")
    private Long entryTimeBegin;

    @ApiModelProperty(name = "entry_time_end", value = "入库时间结束")
    private Long entryTimeEnd;

    @ApiModelProperty(name = "supplier_settlement_flag", value = "是否已生成供应商结算单")
    private Boolean supplierSettlementFlag;

}

