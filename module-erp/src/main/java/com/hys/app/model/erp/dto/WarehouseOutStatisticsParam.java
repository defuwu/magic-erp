package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 出库单统计查询参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class WarehouseOutStatisticsParam extends WarehouseOutQueryParams implements Serializable {

    private static final long serialVersionUID = 6314062358528610522L;

    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "start_time", value = "出库时间-开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "出库时间-截止时间")
    private Long endTime;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "WarehouseOutStatisticsParam{" +
                "categoryId=" + categoryId +
                ", productName='" + productName + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
