package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 合同状态枚举
 *
 * @author 张崧
 * @since 2023-12-05
 */
@Getter
@AllArgsConstructor
public enum ContractStatusEnum {

    /**
     * 未执行
     */
    NEW,
    /**
     * 执行中
     */
    EXECUTING,
    /**
     * 已关闭
     */
    CLOSED
}
