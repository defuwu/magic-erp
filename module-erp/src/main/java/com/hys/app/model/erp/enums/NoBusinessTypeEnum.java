package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 编号生成的业务类型
 *
 * @author 张崧
 * @since 2023-11-30
 */
@Getter
@AllArgsConstructor
public enum NoBusinessTypeEnum {

    /**
     * 入库单
     */
    WarehouseEntry,

    /**
     * 出库单
     */
    WarehouseOut,

    /**
     * 采购计划
     */
    ProcurementPlan,

    /**
     * 采购合同
     */
    ProcurementContract,

    /**
     * 商品借出单
     */
    LendForm,

    /**
     * 入库批次
     */
    WarehouseEntryBatch,

    /**
     * 换货单
     */
    ChangeForm,

    /**
     * 库存报损单
     */
    StockDamageReport,

    /**
     * 调拨单
     */
    StockTransfer,

    /**
     * 供应商退货单
     */
    SupplierReturn,

    /**
     * 订单退货单
     */
    OrderReturn,

    /**
     * 供应商结算单
     */
    SupplierSettlement,

    /**
     * 库存盘点
     */
    StockInventory,

    /**
     * 供应商
     */
    Supplier,

    /**
     * 收款账户
     */
    CollectingAccount,

    /**
     * 订单
     */
    Order,

    /**
     * 财务明细
     */
    FinanceItem,

}
