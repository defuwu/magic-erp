package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单状态枚举
 *
 * @author 张崧
 * @since 2024-01-25
 */
@Getter
@AllArgsConstructor
public enum OrderStatusEnum {

    /**
     * 待提交
     */
    WAIT_SUBMIT("待提交"),
    /**
     * 待审核
     */
    WAIT_AUDIT("待审核"),
    /**
     * 审核驳回
     */
    AUDIT_REJECT("审核驳回"),
    /**
     * 待出库
     */
    WAIT_WAREHOUSE_OUT("待出库"),
    /**
     * 待发货
     */
    WAIT_SHIP("待发货"),
    /**
     * 已完成
     */
    COMPLETE("已完成");

    private final String text;

}
