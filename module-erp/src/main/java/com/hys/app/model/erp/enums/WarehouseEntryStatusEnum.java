package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 入库单状态枚举
 *
 * @author 张崧
 * @since 2023-12-05
 */
@Getter
@AllArgsConstructor
public enum WarehouseEntryStatusEnum {

    /**
     * 未提交
     */
    NotSubmit,
    /**
     * 已提交
     */
    Submit,
    /**
     * 审核通过
     */
    AuditPass,
    /**
     * 审核驳回
     */
    AuditReject,


}
