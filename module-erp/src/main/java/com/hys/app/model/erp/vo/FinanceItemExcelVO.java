package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 财务明细excel导出
 *
 * @author 张崧
 * 2024-03-15 17:40:23
 */
@Data
public class FinanceItemExcelVO {

    @ExcelProperty("财务明细编号")
    private String sn;
    
    @ExcelProperty("来源")
    private String sourceType;
    
    @ExcelProperty("来源编号")
    private String sourceSn;
    
    @ExcelProperty("金额")
    private Double amount;
    
    @ExcelProperty("收入/支出")
    private String amountType;
    
}

