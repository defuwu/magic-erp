package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 商品标签excel导出
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
@Data
public class GoodsLabelExcelVO {

    @ExcelProperty("标签名称")
    private String name;
    
}

