package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 商品标签-商品关联表excel导出
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
@Data
public class GoodsLabelRelationExcelVO {

    @ExcelProperty("标签id")
    private Long labelId;
    
    @ExcelProperty("商品id")
    private Long goodsId;
    
}

