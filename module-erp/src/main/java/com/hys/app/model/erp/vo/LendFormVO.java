package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.LendForm;
import com.hys.app.model.erp.dos.LendFormProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 商品借出单实体VO
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class LendFormVO extends LendForm implements Serializable {

    private static final long serialVersionUID = 350203806591680073L;

    /**
     * 借出单商品
     */
    @ApiModelProperty(name = "product_list", value = "借出单商品")
    private List<LendFormProduct> productList;

    public List<LendFormProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<LendFormProduct> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "LendFormVO{" +
                "productList=" + productList +
                '}';
    }
}
