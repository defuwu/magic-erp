package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 订单excel导出
 *
 * @author 张崧
 * 2024-01-24 15:58:31
 */
@Data
public class OrderExcelVO {

    @ExcelProperty("订单编号")
    private String sn;
    
    @ExcelProperty("订单状态")
    private String status;
    
    @ExcelProperty("会员id")
    private Long memberId;
    
    @ExcelProperty("会员名称")
    private String memberName;
    
    @ExcelProperty("下单时间")
    private Long orderTime;
    
    @ExcelProperty("仓库id")
    private Long warehouseId;
    
    @ExcelProperty("仓库名称")
    private String warehouseName;
    
    @ExcelProperty("销售经理id")
    private Long marketingId;
    
    @ExcelProperty("销售经理名称")
    private String marketingName;
    
    @ExcelProperty("原价总额")
    private Double originPrice;
    
    @ExcelProperty("优惠总额")
    private Double discountPrice;
    
    @ExcelProperty("税额")
    private Double taxPrice;
    
    @ExcelProperty("实付金额")
    private Double actualPrice;
    
    @ExcelProperty("定金金额")
    private Double depositPrice;
    
    @ExcelProperty("附件")
    private String attachment;
    
    @ExcelProperty("备注")
    private String remark;
    
    @ExcelProperty("配送方式")
    private String deliveryType;
    
    @ExcelProperty("自提门店id")
    private Long storeId;
    
    @ExcelProperty("自提门店名称")
    private String storeName;
    
    @ExcelProperty("是否已出库")
    private Boolean warehouseOutFlag;
    
    @ExcelProperty("出库单id")
    private Long warehouseOutId;
    
    @ExcelProperty("是否已发货")
    private Boolean shipFlag;
    
    @ExcelProperty("发货时间")
    private Long shipTime;
    
    @ExcelProperty("物流公司id")
    private Long logisticsCompanyId;
    
    @ExcelProperty("物流公司名称")
    private String logisticsCompanyName;
    
    @ExcelProperty("物流单号")
    private String logisticsTrackingNumber;
    
}

