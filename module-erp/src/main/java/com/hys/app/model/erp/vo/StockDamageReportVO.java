package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StockDamageReport;
import com.hys.app.model.erp.dos.StockDamageReportProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 库存报损单实体VO
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class StockDamageReportVO extends StockDamageReport implements Serializable {

    private static final long serialVersionUID = -5851214268014240143L;

    /**
     * 库存报损单商品信息集合
     */
    @ApiModelProperty(name = "product_list", value = "库存报损单商品信息集合")
    private List<StockDamageReportProduct> productList;

    public List<StockDamageReportProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<StockDamageReportProduct> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "StockDamageReportVO{" +
                "productList=" + productList +
                '}';
    }
}
