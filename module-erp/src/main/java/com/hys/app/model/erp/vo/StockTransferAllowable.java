package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StockTransferDO;
import com.hys.app.model.erp.enums.StockTransferStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 入库单操作
 *
 * @author 张崧
 * @since 2023-12-06
 */
@Data
@NoArgsConstructor
public class StockTransferAllowable {

    @ApiModelProperty(name = "edit", value = "是否允许编辑")
    private Boolean edit;

    @ApiModelProperty(name = "submit", value = "是否允许提交")
    private Boolean submit;

    @ApiModelProperty(name = "withdraw", value = "是否允许撤回")
    private Boolean withdraw;

    @ApiModelProperty(name = "confirm", value = "是否允许确认")
    private Boolean confirm;

    @ApiModelProperty(name = "delete", value = "是否允许删除")
    private Boolean delete;


    public StockTransferAllowable(StockTransferDO stockTransferDO) {
        StockTransferStatusEnum status = stockTransferDO.getStatus();
        if (status == null) {
            return;
        }

        this.setEdit(status == StockTransferStatusEnum.NotSubmit ||
                status == StockTransferStatusEnum.AuditReject);

        this.setSubmit(status == StockTransferStatusEnum.NotSubmit ||
                status == StockTransferStatusEnum.AuditReject);

        this.setDelete(status == StockTransferStatusEnum.NotSubmit ||
                status == StockTransferStatusEnum.AuditReject);

        this.setWithdraw(status == StockTransferStatusEnum.Submit);

        this.setConfirm(status == StockTransferStatusEnum.Submit);
    }

}

