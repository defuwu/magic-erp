package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.SupplierReturnItemDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 供应商退货统计参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class SupplierReturnStatistics extends SupplierReturnItemDO implements Serializable {

    private static final long serialVersionUID = 4580612225167657112L;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;

    @ApiModelProperty(name = "sn", value = "退货单编号")
    private String sn;

    @ApiModelProperty(name = "return_time", value = "退货时间")
    private Long returnTime;

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Long returnTime) {
        this.returnTime = returnTime;
    }

    @Override
    public String toString() {
        return "SupplierReturnStatistics{" +
                "deptName='" + deptName + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", supplierName='" + supplierName + '\'' +
                ", sn='" + sn + '\'' +
                ", returnTime=" + returnTime +
                '}';
    }
}
