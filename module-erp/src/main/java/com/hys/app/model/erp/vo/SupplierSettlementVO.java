package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.SupplierSettlementDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 供应商结算单VO
 *
 * @author 张崧
 * @since 2023-12-15 14:09:09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierSettlementVO extends SupplierSettlementDO {

    @ApiModelProperty(name = "item_list", value = "明细列表")
    private List<SupplierSettlementItemVO> itemList;

    @ApiModelProperty(name = "warehouse_entry_list", value = "入库单列表")
    private List<WarehouseEntryVO> warehouseEntryList;

}

