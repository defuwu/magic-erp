package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseEntryDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 入库单详情
 *
 * @author 张崧
 * @since 2023-12-05 11:25:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryVO extends WarehouseEntryDO {

    @ApiModelProperty(name = "allowable", value = "允许的操作")
    private WarehouseEntryAllowable allowable;

    @ApiModelProperty(name = "product_list", value = "商品列表")
    private List<WarehouseEntryProductVO> productList;

    @ApiModelProperty(name = "batch_list", value = "批次列表")
    private List<WarehouseEntryBatchVO> batchList;

}

