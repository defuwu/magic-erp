package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 出库单出库预览VO
 *
 * @author 张崧
 * @since 2023-12-08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseOutPreviewVO {

    @ApiModelProperty(name = "order_list", value = "订单列表")
    private List<WarehouseOutOrderVO> orderList;


    @Data
    public static class Order {

        @ApiModelProperty(name = "order_id", value = "订单id")
        private Long orderId;

        @ApiModelProperty(name = "order_sn", value = "订单编号")
        private String orderSn;

        @ApiModelProperty(name = "product_list", value = "商品列表")
        private List<WarehouseOutItemVO> productList;

    }

    @Data
    public static class Product {

        @ApiModelProperty(name = "order_id", value = "订单id")
        private Long orderId;

        @ApiModelProperty(name = "order_sn", value = "订单编号")
        private String orderSn;

        @ApiModelProperty(name = "product_list", value = "商品列表")
        private List<WarehouseOutItemVO> productList;

    }


}