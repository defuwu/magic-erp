package com.hys.app.model.errorcode;


/**
 * 系统设置异常
 *
 * @author zh
 * @version v1.0
 * @since v1.0
 * 2018年3月22日 上午10:40:05
 */
public enum SystemErrorCode {

    //上传方案异常编码
    E802("上传图片失败"),
    //上传方案异常编码
    E900("此上传方案已经存在"),
    E901("不允许上传的图片格式"),
    E902("上传图片失败"),
    E903("删除图片失败"),
    E904("发送邮件失败"),
    E913("菜单唯一标识重复"),
    E914("菜单级别最多为3级"),
    E915("管理员名称已经存在"),
    E916("必须保留一个超级管理员"),
    E917("管理员密码不能为空"),
    E918("管理员账号或密码错误"),
    E919("短信平台方案已经存在"),
    E920("必须保留一个开启状态的短信平台"),
    E921("原密码错误"),
    E922("原始密码不能为空"),
    E923("新密码不能为空"),
    E924("角色不能删除"),
    E927("微信服务消息出错"),
    E928("敏感词出错"),
    E930("验证失败"),
    E931("短信接口相关");

    private String describe;

    SystemErrorCode(String des) {
        this.describe = des;
    }

    /**
     * 获取异常码
     *
     * @return
     */
    public String code() {
        return this.name().replaceAll("E", "");
    }


}
