package com.hys.app.model.shoptnt;

import com.hys.app.framework.util.JsonUtil;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 第三方接口请求参数
 *
 * @author 张崧
 * @since v7.2.2
 */
public class ApiParams implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Map<String, Object> params = new HashMap<>();

    public ApiParams put(String key, Object value){
        params.put(key, value);
        return this;
    }

    public String toJson(){
        return JsonUtil.objectToJson(params);
    }

    public Map<String, Object> getParams() {
        return params;
    }
}
