package com.hys.app.model.system.dos;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.system.enums.DeptTypeEnum;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.framework.database.mybatisplus.type.LongListTypeHandler;
import com.hys.app.framework.database.mybatisplus.type.StringListTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 部门表
 */
@TableName(value = "system_dept", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
public class DeptDO extends BaseDO {

    public static final Long ROOT_ID = 0L;
    /**
     * 部门ID
     */
    @TableId
    private Long id;
    /**
     * 部门名称
     */
    private String name;
    /**
     * 部门编码
     */
    private String sn;
    /**
     * 父部门ID
     *
     * 关联 {@link #id}
     */
    private Long parentId;
    /**
     * 所在地区id
     */
    @TableField(typeHandler = LongListTypeHandler.class)
    private List<Long> regionIds;
    /**
     * 所在地区名称
     */
    @TableField(typeHandler = StringListTypeHandler.class)
    private List<String> regionNames;
    /**
     * 部门类型
     */
    private DeptTypeEnum type;
    /**
     * 显示顺序
     */
    private Integer sort;
    /**
     * 部门说明
     */
    private String remark;
}
