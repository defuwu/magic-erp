package com.hys.app.model.system.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * 短信发送记录实体
 *
 * @author zhangsong
 * @version v1.0
 * @since v1.0
 * 2021-02-05 17:17:30
 */
@TableName(value = "es_sms_send_record")
@ApiModel
public class SmsSendRecord implements Serializable {

    private static final long serialVersionUID = 1681549944728068L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 接收短信的手机号
     */
    @ApiModelProperty(name = "phone", value = "接收短信的手机号")
    private String phone;
    /**
     * 发送内容
     */
    @ApiModelProperty(name = "send_content", value = "发送内容")
    private String sendContent;
    /**
     * 业务类型
     */
    @ApiModelProperty(name = "service_type", value = "业务类型")
    private String serviceType;
    /**
     * 发送时间
     */
    @ApiModelProperty(name = "send_time", value = "发送时间")
    private Long sendTime;
    /**
     * 模板id
     */
    @ApiModelProperty(name = "template_id", value = "模板id")
    private Long templateId;
    /**
     * 签名名称
     */
    @ApiModelProperty(name = "sign_name", value = "签名名称")
    private String signName;
    /**
     * 回执ID
     */
    @ApiModelProperty(name = "biz_id", value = "回执ID")
    private String bizId;
    /**
     * 短信发送状态
     */
    @ApiModelProperty(name = "send_status", value = "短信发送状态")
    private String sendStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSendContent() {
        return sendContent;
    }

    public void setSendContent(String sendContent) {
        this.sendContent = sendContent;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SmsSendRecord that = (SmsSendRecord) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(phone, that.phone)
                .append(sendContent, that.sendContent)
                .append(serviceType, that.serviceType)
                .append(sendTime, that.sendTime)
                .append(templateId, that.templateId)
                .append(signName, that.signName)
                .append(bizId, that.bizId)
                .append(sendStatus, that.sendStatus)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(phone)
                .append(sendContent)
                .append(serviceType)
                .append(sendTime)
                .append(templateId)
                .append(signName)
                .append(bizId)
                .append(sendStatus)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "SmsSendRecord{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", sendContent='" + sendContent + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", sendTime=" + sendTime +
                ", templateId=" + templateId +
                ", signName='" + signName + '\'' +
                ", bizId='" + bizId + '\'' +
                ", sendStatus='" + sendStatus + '\'' +
                '}';
    }
}