package com.hys.app.model.system.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.framework.util.DateUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.hys.app.model.support.validator.annotation.LogLevel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 系统日志实体
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:58
 */
@TableName(value = "es_system_logs")
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SystemLogs implements Serializable {

    private static final long serialVersionUID = 2535359233207726L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 请求方法
     */
    @ApiModelProperty(name = "method", value = "请求方法")
    private String method;
    /**
     * 请求参数
     */
    @ApiModelProperty(name = "params", value = "请求参数")
    private String params;
    /**
     * 操作时间
     */
    @ApiModelProperty(name = "operate_time", value = "操作时间")
    private Long operateTime;
    /**
     * 日志级别
     * @see LogLevel
     */
    @ApiModelProperty(name = "level", value = "日志级别")
    private String level;
    /**
     * 端类型：管理端还是商家端
     */
    @ApiModelProperty(name = "client", value = "端类型：管理端还是商家端")
    private String client;
    /**
     * 操作描述
     */
    @ApiModelProperty(name = "operate_detail", value = "操作描述")
    private String operateDetail;
    /**
     * ip地址
     */
    @ApiModelProperty(name = "operate_ip", value = "ip地址")
    private String operateIp;
    /**
     * 操作员
     */
    @ApiModelProperty(name = "operator_name", value = "操作员")
    private String operatorName;
    /**
     * 操作员id，管理员id/会员id
     */
    @ApiModelProperty(name = "operator_id", value = "操作员id，管理员id/会员id")
    private Long operatorId;
    /**
     * 商家id, 店员要存储所属商家，管理端存0
     */
    @ApiModelProperty(name = "seller_id", value = "商家id, 店员要存储所属商家，管理端存0")
    private Long sellerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Long getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Long operateTime) {
        this.operateTime = operateTime;
    }

    public String getOperateDetail() {
        return operateDetail;
    }

    public void setOperateDetail(String operateDetail) {
        this.operateDetail = operateDetail;
    }

    public String getOperateIp() {
        return operateIp;
    }

    public void setOperateIp(String operateIp) {
        this.operateIp = operateIp;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public SystemLogs(String method, String params, String operateDetail, String operateIp, String operatorName, Long operatorId, Long sellerId, String level, String client) {
        this.method = method;
        this.params = params;
        this.operateTime = DateUtil.getDateline();
        this.level = level;
        this.client = client;
        this.operateDetail = operateDetail;
        this.operateIp = operateIp;
        this.operatorName = operatorName;
        this.operatorId = operatorId;
        this.sellerId = sellerId;
    }

    public SystemLogs() {
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SystemLogs that = (SystemLogs) o;
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        if (method != null ? !method.equals(that.method) : that.method != null) {
            return false;
        }
        if (params != null ? !params.equals(that.params) : that.params != null) {
            return false;
        }
        if (operateTime != null ? !operateTime.equals(that.operateTime) : that.operateTime != null) {
            return false;
        }
        if (operateDetail != null ? !operateDetail.equals(that.operateDetail) : that.operateDetail != null) {
            return false;
        }
        if (operateIp != null ? !operateIp.equals(that.operateIp) : that.operateIp != null) {
            return false;
        }
        if (operatorName != null ? !operatorName.equals(that.operatorName) : that.operatorName != null) {
            return false;
        }
        if (operatorId != null ? !operatorId.equals(that.operatorId) : that.operatorId != null) {
            return false;
        }
        if (sellerId != null ? !sellerId.equals(that.sellerId) : that.sellerId != null) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (method != null ? method.hashCode() : 0);
        result = 31 * result + (params != null ? params.hashCode() : 0);
        result = 31 * result + (operateTime != null ? operateTime.hashCode() : 0);
        result = 31 * result + (operateDetail != null ? operateDetail.hashCode() : 0);
        result = 31 * result + (operateIp != null ? operateIp.hashCode() : 0);
        result = 31 * result + (operatorName != null ? operatorName.hashCode() : 0);
        result = 31 * result + (operatorId != null ? operatorId.hashCode() : 0);
        result = 31 * result + (sellerId != null ? sellerId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SystemLogs{" +
                "id=" + id +
                ", method='" + method + '\'' +
                ", params='" + params + '\'' +
                ", operateTime=" + operateTime +
                ", operateDetail='" + operateDetail + '\'' +
                ", operateIp='" + operateIp + '\'' +
                ", operatorName='" + operatorName + '\'' +
                ", operatorId=" + operatorId +
                ", sellerId=" + sellerId +
                '}';
    }
}