package com.hys.app.model.system.dto;

import com.hys.app.model.system.vo.DeptBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@ApiModel(description = "管理后台 - 部门更新 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
public class DeptUpdateDTO extends DeptBaseVO {

    @ApiModelProperty(value = "部门编号", example = "1024")
    @NotNull(message = "部门编号不能为空")
    private Long id;

}
