package com.hys.app.model.system.enums;

/**
 * 审核状态枚举
 *
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:23
 */
public enum AuditStatusEnum {

    /**
     * 待提交审核
     */
    WAITING("待提交审核"),

    /**
     * 已提交审核，审核中
     */
    AUDITING("审核中"),

    /**
     * 审核通过
     */
    PASS("审核通过"),

    /**
     * 审核未通过
     */
    NOT_PASS("审核未通过"),
    /**
     * 助通审核通过
     */
    ZT_PASS("2"),

    /**
     * 助通审核不通过
     */
    ZT_NOT_PASS("3"),

    /**
     * 阿里审核通过
     */
    ALI_PASS("approved"),

    /**
     * 阿里审核不通过
     */
    ALI_NOT_PASS("rejected");


    private String description;

    AuditStatusEnum(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }

    public String value() {
        return this.name();
    }
}
