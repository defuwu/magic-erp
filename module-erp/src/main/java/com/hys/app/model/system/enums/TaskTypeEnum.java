package com.hys.app.model.system.enums;

/**
 * 任务类型
 *
 * @author 张崧
 * @since 2024-01-23
 **/
public enum TaskTypeEnum {
    /**
     * 导入
     */
    Import,
    /**
     * 导出
     */
    Export,

}
