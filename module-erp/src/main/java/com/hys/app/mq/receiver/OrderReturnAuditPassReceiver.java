package com.hys.app.mq.receiver;

import com.hys.app.mq.event.OrderReturnAuditPassEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.erp.dto.message.OrderReturnAuditPassMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 订单退货审核通过
 *
 * @author 张崧
 * @since 2024-01-09
 */
@Component
public class OrderReturnAuditPassReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<OrderReturnAuditPassEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.ORDER_RETURN_AUDIT_PASS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.ORDER_RETURN_AUDIT_PASS, type = ExchangeTypes.FANOUT)
    ))
    public void receive(OrderReturnAuditPassMessage message) {
        if (events != null) {
            for (OrderReturnAuditPassEvent event : events) {
                try {
                    event.onOrderReturnAuditPass(message);
                } catch (Exception e) {
                    logger.error("订单退货审核通过消息出错" + event.getClass().getName(), e);
                }
            }
        }
    }
}
