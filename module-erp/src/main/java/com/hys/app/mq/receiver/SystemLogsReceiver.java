package com.hys.app.mq.receiver;

import com.hys.app.mq.event.SystemLogsEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.system.dos.SystemLogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 系统日志 消息接收者
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
public class SystemLogsReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<SystemLogsEvent> events;

    /**
     * 处理系统日志消息
     *
     * @param log 日志消息
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.LOGS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.LOGS, type = ExchangeTypes.FANOUT)
    ))
    public void add(SystemLogs log) {
        if (events != null) {
            for (SystemLogsEvent event : events) {
                try {
                    event.add(log);
                } catch (Exception e) {
                    logger.error("处理系统日志消息出错", e);
                }
            }
        }
    }
}
