package com.hys.app.service.base.service;

import com.hys.app.model.system.dos.SmsTemplate;

/**
 * 阿里云短信API
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-27
 */
public interface AliSmsApiManager {

    /**
     * 申请短信模板
     * @param model 短信模板实体
     */
    void addSmsTemplate(SmsTemplate model);

    /**
     * 删除短信模板
     * @param templateCode 模板code
     */
    void deleteSmsTemplate(String templateCode);
}