package com.hys.app.service.erp;

import com.hys.app.model.erp.dos.ChangeFormProduct;
import com.hys.app.model.erp.dto.ChangeFormProductDTO;

import java.util.List;

/**
 * 换货单产品业务接口
 * @author dmy
 * 2023-12-05
 */
public interface ChangeFormProductManager {

    /**
     * 新增换货单产品信息
     *
     * @param changeId 换货单ID
     * @param productList 产品信息集合
     * @param type 产品类型 0：退货，1：换货
     */
    void saveProduct(Long changeId, List<ChangeFormProductDTO> productList, Integer type);

    /**
     * 删除换货单产品信息
     *
     * @param changeIds 换货单ID集合
     * @param type 产品类型 0：退货，1：换货
     */
    void deleteProduct(List<Long> changeIds, Integer type);

    /**
     * 根据换货单ID获取换货单产品信息集合
     *
     * @param changeId 换货单ID
     * @param type 产品类型 0：退货，1：换货
     * @return
     */
    List<ChangeFormProduct> list(Long changeId, Integer type);

    /**
     * 修改换货单中的商品库存
     *
     * @param changeId 换货单ID
     * @param warehouseId 仓库ID
     */
    void updateProductStock(Long changeId, Long warehouseId);
}
