package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.EnterpriseDO;
import com.hys.app.model.erp.vo.EnterpriseVO;
import com.hys.app.model.erp.dto.EnterpriseDTO;
import com.hys.app.model.erp.dto.EnterpriseQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 企业业务层接口
 *
 * @author 张崧
 * 2024-03-25 15:44:10
 */
public interface EnterpriseManager extends BaseService<EnterpriseDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<EnterpriseVO> list(EnterpriseQueryParams queryParams);

    /**
     * 添加
     * @param enterpriseDTO 新增数据
     */
    void add(EnterpriseDTO enterpriseDTO);

    /**
     * 编辑
     * @param enterpriseDTO 更新数据
     */
    void edit(EnterpriseDTO enterpriseDTO);

    /**
     * 查询详情
     * @param id 主键
     * @return 详情数据
     */
    EnterpriseVO getDetail(Long id);

    /**
     * 删除
     * @param ids 主键集合
     */
    void delete(List<Long> ids);
}

