package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.LendFormProduct;
import com.hys.app.model.erp.dto.LendFormProductDTO;
import com.hys.app.model.erp.dto.LendFormProductReturnDTO;
import com.hys.app.model.erp.dto.ProductLendNumDTO;

import java.util.List;

/**
 * 借出单产品业务接口
 * @author dmy
 * 2023-12-05
 */
public interface LendFormProductManager {

    /**
     * 新增借出单产品信息
     *
     * @param lendId 借出单ID
     * @param productList 产品信息集合
     */
    void saveProduct(Long lendId, List<LendFormProductDTO> productList);

    /**
     * 删除借出单产品信息
     *
     * @param lendIds 借出单ID集合
     */
    void deleteProduct(List<Long> lendIds);

    /**
     * 根据借出单ID获取借出单产品信息集合
     * @param lendId 借出单ID
     * @return
     */
    List<LendFormProduct> list(Long lendId);

    /**
     * 修改借出单产品归还数量
     *
     * @param productList
     */
    void updateProductReturnNum(List<LendFormProductReturnDTO> productList);

    /**
     * 查询待归还的借出单商品分页列表数据
     *
     * @param pageNo 页数
     * @param pageSize 每页数量
     * @return
     */
    WebPage listWaitReturnProductPage(Long pageNo, Long pageSize);

    /**
     * 查询产品借出数量信息
     *
     * @param lendList 借出数量信息
     * @return
     */
    List<ProductLendNumDTO> listProductLendNum(List<ProductLendNumDTO> lendList);
}
