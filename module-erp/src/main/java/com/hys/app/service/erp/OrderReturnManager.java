package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.OrderReturnDO;
import com.hys.app.model.erp.dto.OrderReturnStatisticsQueryParam;
import com.hys.app.model.erp.enums.OrderReturnStatusEnum;
import com.hys.app.model.erp.vo.OrderReturnVO;
import com.hys.app.model.erp.dto.OrderReturnDTO;
import com.hys.app.model.erp.dto.OrderReturnQueryParams;
import com.hys.app.framework.database.WebPage;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 订单退货业务层接口
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
public interface OrderReturnManager extends BaseService<OrderReturnDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<OrderReturnVO> list(OrderReturnQueryParams queryParams);

    /**
     * 添加
     * @param orderReturnDTO
     */
    void add(OrderReturnDTO orderReturnDTO);

    /**
     * 编辑
     * @param orderReturnDTO
     */
    void edit(OrderReturnDTO orderReturnDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    OrderReturnVO getDetail(Long id);

    /**
     * 删除
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 提交
     * @param id
     */
    void submit(Long id);

    /**
     * 撤销提交
     * @param id
     */
    void withdraw(Long id);

    /**
     * 审核
     * @param ids
     * @param status
     * @param remark
     */
    void audit(List<Long> ids, OrderReturnStatusEnum status, String remark);

    /**
     * 查询订单退货统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage statistics(OrderReturnStatisticsQueryParam params);

    /**
     * 导出订单退货统计列表数据
     *
     * @param response
     * @param params 查询参数
     */
    void export(HttpServletResponse response, OrderReturnStatisticsQueryParam params);
}

