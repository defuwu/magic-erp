package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.PostDO;
import com.hys.app.model.erp.dto.PostDTO;
import com.hys.app.model.erp.dto.PostQueryParams;
import com.hys.app.model.erp.vo.PostVO;

import java.util.List;

/**
 * 岗位业务层接口
 *
 * @author 张崧
 * 2023-11-29 17:15:22
 */
public interface PostManager extends BaseService<PostDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<PostVO> list(PostQueryParams queryParams);

    /**
     * 添加
     *
     * @param postDTO
     */
    void add(PostDTO postDTO);

    /**
     * 编辑
     *
     * @param postDTO
     */
    void edit(PostDTO postDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    PostVO getDetail(Long id);

    /**
     * 删除
     *
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 查询全部岗位
     *
     * @return
     */
    List<PostVO> listAll();
}

