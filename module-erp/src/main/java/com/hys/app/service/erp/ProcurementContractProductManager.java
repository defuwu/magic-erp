package com.hys.app.service.erp;

import com.hys.app.model.erp.dos.ProcurementContractProduct;
import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import com.hys.app.model.erp.dto.ProcurementContractProductDTO;

import java.util.List;

/**
 * 采购合同产品业务接口
 * @author dmy
 * 2023-12-05
 */
public interface ProcurementContractProductManager {

    /**
     * 新增采购合同产品信息
     *
     * @param contractId 合同ID
     * @param productList 产品信息集合
     */
    void saveProduct(Long contractId, List<ProcurementContractProductDTO> productList);

    /**
     * 删除采购合同产品信息
     *
     * @param contractIds 采购合同ID集合
     */
    void deleteProduct(List<Long> contractIds);

    /**
     * 根据采购合同ID获取采购合同产品信息集合
     * @param contractId 采购合同ID
     * @return
     */
    List<ProcurementContractProduct> list(Long contractId);

    /**
     * 更新合同商品的已入库数量
     *
     * @param contractId
     * @param productList
     */
    void updateStockNum(Long contractId, List<WarehouseEntryProductDO> productList);
}
