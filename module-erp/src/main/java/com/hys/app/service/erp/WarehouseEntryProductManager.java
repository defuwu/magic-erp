package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import com.hys.app.model.erp.dto.WarehouseEntryProductDTO;
import com.hys.app.model.erp.dto.WarehouseEntryProductQueryParams;
import com.hys.app.model.erp.vo.WarehouseEntryProductVO;

import java.util.List;

/**
 * 入库单商品明细业务层接口
 *
 * @author 张崧
 * @since 2023-12-05 15:30:10
 */
public interface WarehouseEntryProductManager extends BaseService<WarehouseEntryProductDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<WarehouseEntryProductVO> list(WarehouseEntryProductQueryParams queryParams);

    /**
     * 添加
     *
     * @param warehouseEntryProductDTO
     */
    void add(WarehouseEntryProductDTO warehouseEntryProductDTO);

    /**
     * 编辑
     *
     * @param warehouseEntryProductDTO
     */
    void edit(WarehouseEntryProductDTO warehouseEntryProductDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    WarehouseEntryProductVO getDetail(Long id);

    /**
     * 删除
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 根据入库单id删除
     *
     * @param warehouseEntryId 入库单id
     */
    void deleteByWarehouseEntryId(Long warehouseEntryId);

    /**
     * 根据入库单id批量删除
     *
     * @param warehouseEntryIds
     */
    void deleteByWarehouseEntryIds(List<Long> warehouseEntryIds);

    /**
     * 查询入库单商品明细
     *
     * @param warehouseEntryId
     * @return
     */
    List<WarehouseEntryProductDO> listByWarehouseEntryId(Long warehouseEntryId);

    /**
     * 批量查询入库单商品明细
     * @param warehouseEntryIds
     * @return
     */
    List<WarehouseEntryProductDO> listByWarehouseEntryIds(List<Long> warehouseEntryIds);

    /**
     * 更新退货数量
     * @param id
     * @param returnNum
     */
    void updateReturnNum(Long id, Integer returnNum);
}

