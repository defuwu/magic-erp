package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.WarehouseOutDO;
import com.hys.app.model.erp.dto.*;
import com.hys.app.model.erp.enums.WarehouseOutStatusEnum;
import com.hys.app.model.erp.vo.WarehouseOutVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 出库单业务层接口
 *
 * @author 张崧
 * @since 2023-12-07 16:50:20
 */
public interface WarehouseOutManager extends BaseService<WarehouseOutDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<WarehouseOutVO> list(WarehouseOutQueryParams queryParams);

    /**
     * 添加
     *
     * @param warehouseOutDTO
     */
    void add(WarehouseOutDTO warehouseOutDTO);

    /**
     * 编辑
     *
     * @param warehouseOutDTO
     */
    void edit(WarehouseOutDTO warehouseOutDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    WarehouseOutVO getDetail(Long id);

    /**
     * 删除
     *
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 出库前预览
     *
     * @param warehouseOutPreviewDTO
     * @return
     */
    WarehouseOutVO preview(WarehouseOutPreviewDTO warehouseOutPreviewDTO);

    /**
     * 发货
     *
     * @param shipDTO
     */
    void ship(WarehouseOutShipDTO shipDTO);

    /**
     * 查询出库单统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage statistics(WarehouseOutStatisticsParam params);

    /**
     * 导出出库单统计列表
     *
     * @param response
     * @param params   查询参数
     */
    void export(HttpServletResponse response, WarehouseOutStatisticsParam params);

    /**
     * 根据仓库id查询数量
     *
     * @param warehouseId
     * @return
     */
    long countByWarehouseId(Long warehouseId);

    /**
     * 审核
     *
     * @param ids
     * @param status
     * @param remark
     */
    void audit(List<Long> ids, WarehouseOutStatusEnum status, String remark);
}

