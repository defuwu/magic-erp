package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.OrderReturnItemConverter;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.mapper.erp.OrderReturnItemMapper;
import com.hys.app.model.erp.dos.OrderReturnItemDO;
import com.hys.app.model.erp.vo.OrderReturnItemVO;
import com.hys.app.model.erp.dto.OrderReturnItemDTO;
import com.hys.app.service.erp.OrderReturnItemManager;
import com.hys.app.model.erp.dto.OrderReturnItemQueryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * 订单退货项业务层实现
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
@Service
public class OrderReturnItemManagerImpl extends BaseServiceImpl<OrderReturnItemMapper, OrderReturnItemDO> implements OrderReturnItemManager {

    @Autowired
    private OrderReturnItemConverter converter;

    @Override
    public WebPage<OrderReturnItemVO> list(OrderReturnItemQueryParams queryParams) {
        WebPage<OrderReturnItemDO> webPage = baseMapper.selectPage(queryParams);
        return converter.convert(webPage);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(OrderReturnItemDTO orderReturnItemDTO) {
        check(orderReturnItemDTO);
        save(converter.convert(orderReturnItemDTO));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(OrderReturnItemDTO orderReturnItemDTO) {
        check(orderReturnItemDTO);
        updateById(converter.convert(orderReturnItemDTO));
    }

    @Override
    public OrderReturnItemVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void deleteByOrderReturnId(List<Long> orderReturnIds) {
        lambdaUpdate().in(OrderReturnItemDO::getOrderReturnId, orderReturnIds).remove();
    }

    @Override
    public List<OrderReturnItemDO> listByOrderReturnId(Long orderReturnId) {
        return lambdaQuery().eq(OrderReturnItemDO::getOrderReturnId, orderReturnId).list();
    }

    private void check(OrderReturnItemDTO orderReturnItemDTO) {
        
    }
}

