package com.hys.app.service.erp.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.util.BeanUtil;
import com.hys.app.mapper.erp.ProcurementContractProductMapper;
import com.hys.app.model.erp.dos.ProcurementContractProduct;
import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import com.hys.app.model.erp.dto.ProcurementContractProductDTO;
import com.hys.app.service.erp.ProcurementContractProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 采购合同产品业务接口实现类
 * @author dmy
 * 2023-12-05
 */
@Service
public class ProcurementContractProductManagerImpl extends ServiceImpl<ProcurementContractProductMapper, ProcurementContractProduct> implements ProcurementContractProductManager {

    @Autowired
    private ProcurementContractProductMapper procurementContractProductMapper;

    /**
     * 新增采购合同产品信息
     *
     * @param contractId 合同ID
     * @param productList 产品信息集合
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveProduct(Long contractId, List<ProcurementContractProductDTO> productList) {
        List<Long> contractIds = new ArrayList<>();
        contractIds.add(contractId);
        this.deleteProduct(contractIds);

        //再循环将商品信息入库
        for (ProcurementContractProductDTO productDTO : productList) {
            ProcurementContractProduct product = new ProcurementContractProduct();
            BeanUtil.copyProperties(productDTO, product);
            product.setContractId(contractId);
            //入库数量默认为0
            product.setStockNum(0);
            this.save(product);
        }
    }

    /**
     * 删除采购合同产品信息
     *
     * @param contractIds 采购合同ID集合
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteProduct(List<Long> contractIds) {
        //根据合同ID集合删除旧采购商品信息
        LambdaQueryWrapper<ProcurementContractProduct> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(ProcurementContractProduct::getContractId, contractIds);
        this.procurementContractProductMapper.delete(wrapper);
    }

    /**
     * 根据采购合同ID获取采购合同产品信息集合
     * @param contractId 采购合同ID
     * @return
     */
    @Override
    public List<ProcurementContractProduct> list(Long contractId) {
        return this.lambdaQuery()
                .eq(ProcurementContractProduct::getContractId, contractId)
                .list();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStockNum(Long contractId, List<WarehouseEntryProductDO> productList) {
        for (WarehouseEntryProductDO entryProductDO : productList) {
            lambdaUpdate()
                    .setSql("stock_num = stock_num + " + entryProductDO.getNum())
                    .eq(ProcurementContractProduct::getContractId, contractId)
                    .eq(ProcurementContractProduct::getProductId, entryProductDO.getProductId())
                    // 走cas逻辑校验，已入库数量不能大于合同数量
                    .geSql(ProcurementContractProduct::getNum, "stock_num + " + entryProductDO.getNum())
                    .update();
//            if(!update){
//                throw new ServiceException("商品：" + entryProductDO.getName() + "的入库数量超过了合同数量");
//            }
        }
    }
}
