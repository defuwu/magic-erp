package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.StoreConverter;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.mapper.erp.StoreMapper;
import com.hys.app.model.erp.dos.StoreDO;
import com.hys.app.model.erp.vo.StoreVO;
import com.hys.app.model.erp.dto.StoreDTO;
import com.hys.app.service.erp.StoreManager;
import com.hys.app.model.erp.dto.StoreQueryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 门店业务层实现
 *
 * @author 张崧
 * @since 2023-12-11 17:47:09
 */
@Service
public class StoreManagerImpl extends BaseServiceImpl<StoreMapper, StoreDO> implements StoreManager {

    @Autowired
    private StoreConverter converter;

    @Override
    public WebPage<StoreVO> list(StoreQueryParams queryParams) {
        WebPage<StoreDO> webPage = baseMapper.selectPage(queryParams);
        return converter.convert(webPage);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(StoreDTO storeDTO) {
        StoreDO storeDO = converter.convert(storeDTO);
        storeDO.setCreateTime(DateUtil.getDateline());
        storeDO.setDeleteFlag(0);
        save(storeDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(StoreDTO storeDTO) {
        updateById(converter.convert(storeDTO));
    }

    @Override
    public StoreVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        lambdaUpdate().set(StoreDO::getDeleteFlag, 1).eq(StoreDO::getId, id).update();
    }
    
}

