package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.SupplierSettlementItemConverter;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.mapper.erp.SupplierSettlementItemMapper;
import com.hys.app.model.erp.dos.SupplierSettlementItemDO;
import com.hys.app.model.erp.vo.SupplierSettlementItemVO;
import com.hys.app.model.erp.dto.SupplierSettlementItemDTO;
import com.hys.app.service.erp.SupplierSettlementItemManager;
import com.hys.app.model.erp.dto.SupplierSettlementItemQueryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * 供应商结算单明细业务层实现
 *
 * @author 张崧
 * @since 2023-12-15 14:09:20
 */
@Service
public class SupplierSettlementItemManagerImpl extends BaseServiceImpl<SupplierSettlementItemMapper, SupplierSettlementItemDO> implements SupplierSettlementItemManager {

    @Autowired
    private SupplierSettlementItemConverter converter;

    @Override
    public WebPage<SupplierSettlementItemVO> list(SupplierSettlementItemQueryParams queryParams) {
        WebPage<SupplierSettlementItemDO> webPage = baseMapper.selectPage(queryParams);
        return converter.convert(webPage);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(SupplierSettlementItemDTO supplierSettlementItemDTO) {
        check(supplierSettlementItemDTO);
        save(converter.convert(supplierSettlementItemDTO));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(SupplierSettlementItemDTO supplierSettlementItemDTO) {
        check(supplierSettlementItemDTO);
        updateById(converter.convert(supplierSettlementItemDTO));
    }

    @Override
    public SupplierSettlementItemVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public void deleteBySettlementId(Long settlementId) {
        lambdaUpdate().eq(SupplierSettlementItemDO::getSupplierSettlementId, settlementId).remove();
    }

    @Override
    public List<SupplierSettlementItemDO> listBySupplierSettlementId(Long settlementId) {
        return lambdaQuery().eq(SupplierSettlementItemDO::getSupplierSettlementId, settlementId).list();
    }

    private void check(SupplierSettlementItemDTO supplierSettlementItemDTO) {
        
    }
}

