package com.hys.app.service.goods;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hys.app.model.goods.dos.GoodsParamsDO;
import com.hys.app.model.goods.vo.GoodsParamsGroupVO;

import java.util.List;

/**
 * 商品参数关联接口
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月21日 下午5:29:09
 */
public interface GoodsParamsManager extends IService<GoodsParamsDO> {

    /**
     * 添加商品关联的参数
     *
     * @param goodsId   商品id
     * @param paramList 参数集合
     */
    void addParams(List<GoodsParamsDO> paramList, Long goodsId);

    /**
     * 修改商品查询分类和商品关联的参数
     *
     * @param categoryId 分类id
     * @param goodsId 商品id
     * @return 商品参数集合
     */
    List<GoodsParamsGroupVO> queryGoodsParams(Long categoryId, Long goodsId);

    /**
     * 添加商品查询分类和商品关联的参数
     *
     * @param categoryId 分类id
     * @return 商品参数集合
     */
    List<GoodsParamsGroupVO> queryGoodsParams(Long categoryId);

    /**
     * 根据商品ID查询商品参数信息集合
     * @param goodsId 商品ID
     * @return
     */
    List<GoodsParamsDO> listParamsByGoodsId(Long goodsId);

}
