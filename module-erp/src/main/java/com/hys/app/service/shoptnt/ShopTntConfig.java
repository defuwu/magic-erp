package com.hys.app.service.shoptnt;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * ShopTnt相关配置
 *
 * @author 张崧
 * @since 2024-04-02
 */
@Configuration
@ConfigurationProperties(prefix = "shop-tnt")
@Data
public class ShopTntConfig {

    /**
     * 接口地址
     */
    private String httpAddress;

    /**
     * 签名密钥
     */
    private String signSecret;

}
