package com.hys.app.service.system;

import cn.hutool.core.collection.CollUtil;
import com.hys.app.model.system.dto.DeptCreateDTO;
import com.hys.app.model.system.dto.DeptQueryParams;
import com.hys.app.model.system.dto.DeptUpdateDTO;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.base.context.Region;
import com.hys.app.model.system.dos.DeptDO;
import com.hys.app.framework.util.CollectionUtils;

import java.util.*;

/**
 * 部门 Service 接口
 */
public interface DeptManager extends BaseService<DeptDO> {

    /**
     * 创建部门
     *
     * @param reqVO  部门信息
     * @param region
     * @return 部门编号
     */
    Long createDept(DeptCreateDTO reqVO, Region region);

    /**
     * 更新部门
     *
     * @param reqVO  部门信息
     * @param region
     */
    void updateDept(DeptUpdateDTO reqVO, Region region);

    /**
     * 删除部门
     *
     * @param id 部门编号
     */
    void deleteDept(Long id);

    /**
     * 获得部门信息
     *
     * @param id 部门编号
     * @return 部门信息
     */
    DeptDO getDept(Long id);

    /**
     * 获得部门信息数组
     *
     * @param ids 部门编号数组
     * @return 部门信息数组
     */
    List<DeptDO> getDeptList(Collection<Long> ids);

    /**
     * 筛选部门列表
     *
     * @param reqVO 筛选条件请求 VO
     * @return 部门列表
     */
    List<DeptDO> getDeptList(DeptQueryParams reqVO);

    /**
     * 获得指定编号的部门 Map
     *
     * @param ids 部门编号数组
     * @return 部门 Map
     */
    default Map<Long, DeptDO> getDeptMap(Collection<Long> ids) {
        if (CollUtil.isEmpty(ids)) {
            return Collections.emptyMap();
        }
        List<DeptDO> list = getDeptList(ids);
        return CollectionUtils.convertMap(list, DeptDO::getId);
    }

    /**
     * 获得指定部门的所有子部门
     *
     * @param id 部门编号
     * @return 子部门列表
     */
    List<DeptDO> getChildDeptList(Long id);

    /**
     * 获得所有子部门，从缓存中
     *
     * @param id 父部门编号
     * @return 子部门列表
     */
    Set<Long> getChildDeptIdListFromCache(Long id);
}
