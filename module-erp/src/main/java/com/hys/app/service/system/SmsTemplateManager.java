package com.hys.app.service.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.SmsTemplate;
import com.hys.app.model.system.enums.EnableStatusEnum;
import com.hys.app.model.system.enums.SmsServiceCodeEnum;

import java.util.Map;

/**
 * 短信模板业务层
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:23
 */
public interface SmsTemplateManager {

	/**
	 * 查询短信模板列表
	 * @param page 页码
	 * @param pageSize 每页数量
	 * @param serviceType 模板业务类型
	 * @param beanId 短信平台beanId
	 * @return WebPage
	 */
     WebPage list(Long page, Long pageSize, String serviceType, String beanId);

	/**
	* 修改短信模板
	* @param smsTemplate 短信模板
	* @param id 短信模板主键
	* @return SmsTemplate 短信模板
	*/
	SmsTemplate edit(SmsTemplate smsTemplate, Long id);
	
	/**
	 * 获取短信模板
	 * @param id 短信模板主键
	 * @return SmsTemplate  短信模板
	 */
	SmsTemplate getModel(Long id);

	/**
	 * 获取短信模板
	 * @param templateCode 短信模板编号
	 * @return SmsTemplate  短信模板
	 */
	SmsTemplate getModel(String templateCode);

	/**
	 * 修改短信模板开启状态
	 * @param id 短信模板id
	 * @param enableStatus 要修改的状态 {@link EnableStatusEnum}
	 */
	void updateEnableStatus(Long id, String enableStatus);

	/**
	 * 提交审核
	 * @param id 短信模板id
	 * @param remark 申请说明(描述业务使用场景)
	 */
	void audit(Long id, String remark);


	/**
	 * 短信模板审核回调
	 * @param josn  回调入参
	 * @param type  回调插件类型
	 * @return
	 */
	Map templateAuditCallback(String josn, String type);

	/**
	 * 查询当前开启的短信平台的模板
	 * @param serviceCode 业务类型编号
	 * @return 短信模板
	 */
	SmsTemplate getOpenTemplate(SmsServiceCodeEnum serviceCode);


	/**
	 * 发送短信回调
	 * @param json
	 * @param type
	 * @return
	 */
    Map smsSendCallback(String json, String type);
}