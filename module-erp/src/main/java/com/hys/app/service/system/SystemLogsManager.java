package com.hys.app.service.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.SystemLogs;
import com.hys.app.model.system.dto.SystemLogsParam;

/**
 * 系统日志业务层
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:58
 */
public interface SystemLogsManager {

	/**
	 * 查询系统日志列表
	 * @param param 查询参数
	 * @param client 客户端
	 * @return WebPage
	 */
     WebPage list(SystemLogsParam param, String client);
	/**
	 * 添加系统日志
	 * @param systemLogs 系统日志
	 * @return SystemLogs 系统日志
	 */
	SystemLogs add(SystemLogs systemLogs);

	/**
	 * 删除系统日志
	 * @param id 系统日志主键
	 */
	void delete(Long id);
	
	/**
	 * 获取系统日志
	 * @param id 系统日志主键
	 * @return SystemLogs  系统日志
	 */
	SystemLogs getModel(Long id);

	/**
	 * 删除小于某个时间的日志信息
	 *
	 * @param time 某个时间
	 */
	void deleteByTime(Long time);

}