package com.hys.app.service.system.impl;

/**
 * 随机验证码生成
 *
 * @author zh
 * @version v7.0
 * @date 18/4/24 下午8:06
 * @since v7.0
 */

public class RandomCreate {

    public static String getRandomCode() {
        // 随机生成的动态码
        return  "" + (int) ((Math.random() * 9 + 1) * 100000);
    }

}
