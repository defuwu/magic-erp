package com.hys.app.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hys.app.framework.datapermission.annotation.DataPermission;
import com.hys.app.framework.util.PageConvert;
import com.hys.app.mapper.system.AdminUserMapper;
import com.hys.app.mapper.system.RoleMapper;
import com.hys.app.model.base.CachePrefix;
import com.hys.app.model.system.dos.AdminUser;
import com.hys.app.model.system.dto.DataPermissionDTO;
import com.hys.app.model.system.enums.DataPermissionScopeEnum;
import com.hys.app.service.system.AdminUserManager;
import com.hys.app.service.system.RoleManager;
import com.hys.app.model.errorcode.SystemErrorCode;
import com.hys.app.model.system.dos.RoleDO;
import com.hys.app.model.system.vo.Menus;
import com.hys.app.model.system.vo.RoleVO;
import com.hys.app.framework.cache.Cache;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ResourceNotFoundException;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.util.JsonUtil;
import com.hys.app.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 角色表业务类
 *
 * @author kingapex
 * @version v1.0.0
 * @since v7.0.0
 * 2018-04-17 16:48:27
 */
@Service
public class RoleManagerImpl extends ServiceImpl<RoleMapper, RoleDO> implements RoleManager {

    @Autowired
    private Cache cache;
    @Autowired
    private AdminUserMapper adminUserMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private AdminUserManager adminUserManager;

    /**
     * 查询角色表列表
     *
     * @param page     页码
     * @param pageSize 每页数量
     * @param keyword  搜索关键字
     * @return WebPage
     */
    @Override
    public WebPage list(long page, long pageSize, String keyword) {

        QueryWrapper<RoleDO> wrapper = new QueryWrapper<>();
        wrapper.select("role_id", "role_name", "role_describe");
        wrapper.like(StringUtil.notEmpty(keyword), "role_name", keyword);
        wrapper.orderByDesc("role_id ");
        IPage<RoleDO> iPage = roleMapper.selectPage(new Page<>(page, pageSize), wrapper);
        return PageConvert.convert(iPage);
    }

    /**
     * 添加角色表
     *
     * @param role 角色表
     * @return Role 角色表
     */
    @Override
    @Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public RoleVO add(RoleVO role) {

        this.checkRole(role);
        RoleDO roleDO = new RoleDO();
        roleDO.setRoleName(role.getRoleName());
        roleDO.setAuthIds(JsonUtil.objectToJson(role.getMenus()));
        roleDO.setRoleDescribe(role.getRoleDescribe());
        roleDO.setDataScope(role.getDataScope());
        roleDO.setDeptIds(role.getDeptIds());

        roleMapper.insert(roleDO);
        role.setRoleId(roleDO.getRoleId());

        //删除缓存中角色所拥有的菜单权限
        cache.remove(CachePrefix.ADMIN_URL_ROLE.getPrefix());
        return role;
    }

    /**
     * 检测角色信息是否合法
     *
     * @param role
     */
    private void checkRole(RoleVO role) {

        Long id = role.getRoleId();

        QueryWrapper<RoleDO> wrapper = new QueryWrapper<>();

        //添加
        wrapper.eq(id == null, "role_name", role.getRoleName());

        wrapper.eq("role_name", role.getRoleName());
        wrapper.ne("role_id", id);


        List list = roleMapper.selectList(wrapper);

        if (list.size() > 0) {
            throw new ServiceException(SystemErrorCode.E924.code(), "角色名称重复");
        }

        if(role.getDataScope() == DataPermissionScopeEnum.DEPT_CUSTOM && role.getDeptIds() == null){
            role.setDeptIds("");
        }
    }

    /**
     * 修改角色表
     *
     * @param role 角色表
     * @param id   角色表主键
     * @return Role 角色表
     */
    @Override
    @Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public RoleVO edit(RoleVO role, Long id) {
        //校验权限是否存在
        RoleDO roleDO = this.getModel(id);
        if (roleDO == null) {
            throw new ResourceNotFoundException("此角色不存在");
        }
        role.setRoleId(id);
        this.checkRole(role);

        roleDO.setRoleName(role.getRoleName());
        roleDO.setAuthIds(JsonUtil.objectToJson(role.getMenus()));
        roleDO.setRoleDescribe(role.getRoleDescribe());
        roleDO.setDataScope(role.getDataScope());
        roleDO.setDeptIds(role.getDeptIds());
        roleMapper.updateById(roleDO);
        role.setRoleId(id);
        //删除缓存中角色所拥有的菜单权限
        cache.remove(CachePrefix.ADMIN_URL_ROLE.getPrefix());
        return role;
    }

    /**
     * 删除角色表
     *
     * @param id 角色表主键
     */
    @Override
    @Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Long id) {
        RoleDO roleDO = this.getModel(id);
        if (roleDO == null) {
            throw new ResourceNotFoundException("此角色不存在");
        }

        //查看角色下是否有管理员，有管理员则不能删除
        QueryWrapper<AdminUser> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", id).ne("user_state", -1);
        List list = adminUserMapper.selectList(wrapper);

        if (StringUtil.isNotEmpty(list)) {
            throw new ServiceException(SystemErrorCode.E924.code(), "该角色下有管理员，请删除管理员后再删除角色");
        }

        roleMapper.deleteById(id);
    }

    /**
     * 获取角色表
     *
     * @param id 角色表主键
     * @return Role  角色表
     */
    @Override
    public RoleDO getModel(Long id) {

        RoleDO roleDO = roleMapper.selectById(id);
        if (roleDO == null) {
            throw new ResourceNotFoundException("此角色不存在");
        }
        return roleDO;
    }

    /**
     * 获取所有角色的权限对照表
     *
     * @return 权限对照表
     */
    @Override
    public Map<String, List<String>> getRoleMap() {

        Map<String, List<String>> roleMap = new HashMap<>(16);

        QueryWrapper<RoleDO> wrapper = new QueryWrapper<>();
        List<RoleDO> roles = roleMapper.selectList(wrapper);
        RoleDO roleDO = this.mockFranchiseRole();
        roles.add(roleDO);

        for (int i = 0; i < roles.size(); i++) {
            List<Menus> menusList = JsonUtil.jsonToList(roles.get(i).getAuthIds(), Menus.class);
            if (menusList != null && menusList.size() > 0) {
                List<String> authList = new ArrayList<>();
                //递归查询角色所拥有的菜单权限
                this.getChildren(menusList, authList);
                roleMap.put(roles.get(i).getRoleName(), authList);
                cache.put(CachePrefix.ADMIN_URL_ROLE.getPrefix(), roleMap);
            }
        }
        return roleMap;
    }

    /**
     * 递归将此角色锁拥有的菜单权限保存到list
     *
     * @param menuList 菜单集合
     * @param authList 权限组集合
     */
    private void getChildren(List<Menus> menuList, List<String> authList) {
        for (Menus menus : menuList) {
            //将此角色拥有的菜单权限放入list中
            if (menus.isChecked()) {
                authList.add(menus.getAuthRegular());
            }
            if (!menus.getChildren().isEmpty()) {
                getChildren(menus.getChildren(), authList);
            }
        }
    }

    /**
     * 根据角色id获取所属菜单
     *
     * @param id 角色id
     * @return 菜单唯一标识
     */
    @Override
    public List<String> getRoleMenu(Long id) {


        //******************************************************
        //新零售逻辑
        //如果是加盟商管理员,则返回固定的菜单
        //******************************************************
        RoleDO roleDO;
        if (id == -1) {
            roleDO = this.mockFranchiseRole();
        } else {
            roleDO = this.getModel(id);
        }
        if (roleDO == null) {
            throw new ResourceNotFoundException("此角色不存在");
        }
        List<Menus> menusList = JsonUtil.jsonToList(roleDO.getAuthIds(), Menus.class);
        List<String> authList = new ArrayList<>();
        //筛选菜单
        this.reset(menusList, authList);
        return authList;
    }

    @Override
    @DataPermission(enable = false)
    public DataPermissionDTO getDataPermission(Long adminUserId) {
        // 默认可以查看自己的数据
        DataPermissionDTO result = new DataPermissionDTO();
        result.setSelf(true);

        // 获取用户的部门id    todo 改为本地缓存
        AdminUser adminUser = adminUserManager.getModel(adminUserId);
        if(adminUser.getRoleId() == 0){
            result.setAll(true);
            return result;
        }

        // 获得用户的角色      todo 改为本地缓存
        RoleDO role = getModel(adminUser.getRoleId());

        // 情况一，全部数据权限
        if (role.getDataScope() == DataPermissionScopeEnum.ALL) {
            result.setAll(true);
        }
        // 情况二，指定部门数据权限
        if (role.getDataScope() == DataPermissionScopeEnum.DEPT_CUSTOM) {
            // 指定部门的id集合
            Set<Long> deptIds = Arrays.stream(role.getDeptIds().split(","))
                    .filter(StringUtil::notEmpty).map(Long::valueOf).collect(Collectors.toSet());
            result.setDeptIds(deptIds);
            // 可以看到自己所在部门的数据，防止看不到自己发布的单据
            deptIds.add(adminUser.getDeptId());
        }
        return result;
    }

    /**
     * 模拟一个供应商的角色
     * 这个角色在数据库中不存在，为了配合虚拟出来的门店角色
     *
     * @return
     */
    private RoleDO mockFranchiseRole() {
        RoleDO roleDO = new RoleDO();
        roleDO.setRoleId(-1L);
        roleDO.setRoleName("供应商");
        roleDO.setAuthIds("[{\"title\":\"设置与维护\",\"identifier\":\"setting\",\"checked\":false,\"authRegular\":\"admin/systems.*\",\"children\":[{\"title\":\"消息设置\",\"identifier\":\"messageSettings\",\"checked\":false,\"authRegular\":\"/admin/system.*\",\"children\":[{\"title\":\"店铺消息\",\"identifier\":\"shopMessage\",\"checked\":false,\"authRegular\":\"/admin/systems/message-templates.*\",\"children\":[]},{\"title\":\"会员消息\",\"identifier\":\"memberMessage\",\"checked\":false,\"authRegular\":\"/admin/systems/message-templates.*\",\"children\":[]},{\"title\":\"微信消息\",\"identifier\":\"wechatMessage\",\"checked\":false,\"authRegular\":\"/admin/systems/wechat-msg-tmp.*\",\"children\":[]}]},{\"title\":\"维护\",\"identifier\":\"maintain\",\"checked\":false,\"authRegular\":\"admin/systems.*\",\"children\":[{\"title\":\"微页面\",\"identifier\":\"mobilePages\",\"checked\":false,\"authRegular\":\"/admin/pages.*\",\"children\":[]},{\"title\":\"静态页\",\"identifier\":\"staticPage\",\"checked\":false,\"authRegular\":\"(/admin/task.*)|(/admin/page-create.*)\",\"children\":[]},{\"title\":\"商品索引\",\"identifier\":\"goodsIndex\",\"checked\":false,\"authRegular\":\"(/admin/task.*)|(/admin/goods/search.*)\",\"children\":[]},{\"title\":\"门店索引\",\"identifier\":\"branchIndex\",\"checked\":false,\"authRegular\":\"(/admin/branch/search.*)\",\"children\":[]},{\"title\":\"热门关键字\",\"identifier\":\"hotKeyword\",\"checked\":false,\"authRegular\":\"/admin/pages/hot-keywords.*\",\"children\":[]},{\"title\":\"敏感词\",\"identifier\":\"sensitiveWords\",\"checked\":false,\"authRegular\":\"/admin/sensitive-words.*\",\"children\":[]},{\"title\":\"菜单管理\",\"identifier\":\"menuManage\",\"checked\":false,\"authRegular\":\"/admin/systems/menus.*\",\"children\":[]},{\"title\":\"文章列表\",\"identifier\":\"articleList\",\"checked\":false,\"authRegular\":\"(/admin/systems/admin-users.*)|(/admin/pages.*)\",\"children\":[]},{\"title\":\"文章分类\",\"identifier\":\"articleCategory\",\"checked\":false,\"authRegular\":\"/admin/pages/article-categories.*\",\"children\":[]},{\"title\":\"搜索分词\",\"identifier\":\"searchKeyword\",\"checked\":false,\"authRegular\":\"/admin/goodssearch.*\",\"children\":[]},{\"title\":\"搜索历史\",\"identifier\":\"searchHistory\",\"checked\":false,\"authRegular\":\"/admin/goodssearch.*\",\"children\":[]},{\"title\":\"搜索提示词\",\"identifier\":\"searchTips\",\"checked\":false,\"authRegular\":\"/admin/goodssearch.*\",\"children\":[]}]},{\"title\":\"系统参数\",\"identifier\":\"systemParams\",\"checked\":false,\"authRegular\":\"(/admin/settings.*)|(/admin/goods.*)|(/admin/trade.*)\",\"children\":[{\"title\":\"区域管理\",\"identifier\":\"regionalManagement\",\"checked\":false,\"authRegular\":\"/regions.*\",\"children\":[]},{\"title\":\"系统设置\",\"identifier\":\"systemSettings\",\"checked\":false,\"authRegular\":\"(/admin/settings.*)|(/admin/goods.*)|(/admin/trade.*)\",\"children\":[]},{\"title\":\"SMTP设置\",\"identifier\":\"smtpSettings\",\"checked\":false,\"authRegular\":\"/admin/systems/smtps.*\",\"children\":[]},{\"title\":\"短信网关\",\"identifier\":\"smsGatewaySettings\",\"checked\":false,\"authRegular\":\"/admin/systems/platforms.*\",\"children\":[]},{\"title\":\"验证平台\",\"identifier\":\"validatorPlatformSettings\",\"checked\":false,\"authRegular\":\"/admin/systems/validator.*\",\"children\":[]},{\"title\":\"储存方案\",\"identifier\":\"storageSolution\",\"checked\":false,\"authRegular\":\"/admin/systems/uploaders.*\",\"children\":[]}]},{\"title\":\"支付与物流\",\"identifier\":\"paymentAndDelivery\",\"checked\":false,\"authRegular\":\"/admin/system.*\",\"children\":[{\"title\":\"快递平台\",\"identifier\":\"expressPlatformSettings\",\"checked\":false,\"authRegular\":\"/admin/systems/express-platforms.*\",\"children\":[]},{\"title\":\"电子面单\",\"identifier\":\"electronicrEceipt\",\"checked\":false,\"authRegular\":\"/admin/systems/waybills.*\",\"children\":[]},{\"title\":\"三方应用\",\"identifier\":\"trustLogin\",\"checked\":false,\"authRegular\":\"/admin/members/connect.*\",\"children\":[]},{\"title\":\"支付方式\",\"identifier\":\"payment\",\"checked\":false,\"authRegular\":\"(/base-api/uploaders.*)|(/admin/payment/payment-methods.*)\",\"children\":[]},{\"title\":\"物流费用\",\"identifier\":\"logisticsManage\",\"checked\":false,\"authRegular\":\"/admin/express.*\",\"children\":[]}]}]},{\"title\":\"智慧导购\",\"identifier\":\"privateSphere\",\"checked\":false,\"authRegular\":\"//TODO\",\"children\":[{\"title\":\"导购\",\"identifier\":\"shopGuide\",\"checked\":false,\"authRegular\":\"/admin/shoppingguide.*\",\"children\":[]}]},{\"title\":\"售前售后\",\"identifier\":\"sale\",\"checked\":false,\"authRegular\":\"/admin/aftersale.*\",\"children\":[{\"title\":\"站内消息\",\"identifier\":\"notificationHistory\",\"checked\":false,\"authRegular\":\"/admin/systems/messages.*\",\"children\":[]}]},{\"title\":\"统计\",\"identifier\":\"statistics\",\"checked\":true,\"authRegular\":\"/admin/statistics.*\",\"children\":[{\"title\":\"会员分析\",\"identifier\":\"memberAnalysis\",\"checked\":true,\"authRegular\":\"/admin/statistics.*\",\"children\":[{\"title\":\"会员下单量\",\"identifier\":\"orderAmount\",\"checked\":true,\"authRegular\":\"(/admin/statistics.*)|(/admin/shops/list)\",\"children\":[]},{\"title\":\"新增会员\",\"identifier\":\"addedMember\",\"checked\":true,\"authRegular\":\"/admin/statistics.*\",\"children\":[]}]},{\"title\":\"商品统计\",\"identifier\":\"goodsStatistics\",\"checked\":true,\"authRegular\":\"/admin/statistic.*\",\"children\":[{\"title\":\"价格销量\",\"identifier\":\"priceSales\",\"checked\":true,\"authRegular\":\"(/admin/shops.*)|(/admin/goods.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"热卖商品\",\"identifier\":\"hotGoods\",\"checked\":true,\"authRegular\":\"(/admin/shops.*)|(/admin/goods.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"销售明细\",\"identifier\":\"goodsSalesDetails\",\"checked\":true,\"authRegular\":\"(/admin/shops.*)|(/admin/goods.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"商品收藏\",\"identifier\":\"goodsCollect\",\"checked\":true,\"authRegular\":\"(/admin/shops.*)|(/admin/goods.*)|(/admin/statistics.*)\",\"children\":[]}]},{\"title\":\"行业分析\",\"identifier\":\"industryAnalysis\",\"checked\":false,\"authRegular\":\"/admin/statistic.*\",\"children\":[{\"title\":\"行业规模\",\"identifier\":\"industryScale\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"概括总览\",\"identifier\":\"generalityOverview\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/statistics.*)\",\"children\":[]}]},{\"title\":\"流量分析\",\"identifier\":\"trafficAnalysis\",\"checked\":false,\"authRegular\":\"/admin/statistics.*\",\"children\":[{\"title\":\"店铺流量\",\"identifier\":\"trafficAnalysisShop\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"商品流量\",\"identifier\":\"trafficAnalysisGoods\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/statistics.*)\",\"children\":[]}]},{\"title\":\"其他统计\",\"identifier\":\"otherStatistics\",\"checked\":false,\"authRegular\":\"/admin/statistics.*\",\"children\":[{\"title\":\"订单\",\"identifier\":\"orderStatistics\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"销售收入\",\"identifier\":\"salesRevenueStatistics\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"区域分析\",\"identifier\":\"regionalAnalysis\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"客单价分布\",\"identifier\":\"customerPriceDistribution\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/goods/categories.*)|(/admin/statistics.*)\",\"children\":[]},{\"title\":\"退款\",\"identifier\":\"refundStatistics\",\"checked\":false,\"authRegular\":\"(/admin/shops.*)|(/admin/statistics/order/return.*)\",\"children\":[]}]}]},{\"title\":\"企微scrm\",\"identifier\":\"private-sphere\",\"checked\":false,\"authRegular\":\"2\",\"children\":[{\"title\":\"引流获客\",\"identifier\":\"drainageGuest\",\"checked\":false,\"authRegular\":\"/admin/fission.*\",\"children\":[{\"title\":\"新客欢迎语\",\"identifier\":\"CustomerWelcome\",\"checked\":false,\"authRegular\":\"/admin/welcome-messages.*\",\"children\":[]},{\"title\":\"进群欢迎语\",\"identifier\":\"WelcomeGroup\",\"checked\":false,\"authRegular\":\"/admin/group/welcome/messages.*\",\"children\":[]},{\"title\":\"加友统计\",\"identifier\":\"addFriendsTask\",\"checked\":false,\"authRegular\":\"/admin/guide-friends.*\",\"children\":[]},{\"title\":\"渠道活码\",\"identifier\":\"channelLiveCode\",\"checked\":false,\"authRegular\":\"/admin/guide/channel-code.*\",\"children\":[]},{\"title\":\"好友裂变\",\"identifier\":\"buddyFission\",\"checked\":false,\"authRegular\":\"/admin/fission.*\",\"children\":[]}]},{\"title\":\"促活转化\",\"identifier\":\"promoteTransformation\",\"checked\":false,\"authRegular\":\"/admin/plant/grass.*\",\"children\":[{\"title\":\"专题页\",\"identifier\":\"mobileSpecialPages\",\"checked\":false,\"authRegular\":\"/admin/pages.*\",\"children\":[]},{\"title\":\"客户群发\",\"identifier\":\"notifyManagement\",\"checked\":false,\"authRegular\":\"/admin/group-message.*\",\"children\":[]},{\"title\":\"企业话术\",\"identifier\":\"privateEnterpriseWordsArts\",\"checked\":false,\"authRegular\":\"/admin/speech-type.*\",\"children\":[{\"title\":\"企业话术\",\"identifier\":\"privateEnterpriseWordsArt\",\"checked\":false,\"authRegular\":\"/admin/speech\",\"children\":[]}]},{\"title\":\"话术分类\",\"identifier\":\"wordsClassifty\",\"checked\":false,\"authRegular\":\"/admin/speech-type.*\",\"children\":[]},{\"title\":\"种草\",\"identifier\":\"privateGrass\",\"checked\":false,\"authRegular\":\"/admin/plant/grass.*\",\"children\":[]},{\"title\":\"导购海报\",\"identifier\":\"shoppersPosters\",\"checked\":false,\"authRegular\":\"/admin/guide/poster-resource.*\",\"children\":[]},{\"title\":\"个人SOP\",\"identifier\":\"personalSop\",\"checked\":false,\"authRegular\":\"/admin/sop.*\",\"children\":[]},{\"title\":\"群SOP\",\"identifier\":\"groupSop\",\"checked\":false,\"authRegular\":\"/admin/sop.*\",\"children\":[]}]},{\"title\":\"社区运营\",\"identifier\":\"communityOperating\",\"checked\":false,\"authRegular\":\"/admin/community.*\",\"children\":[{\"title\":\"标签拉群\",\"identifier\":\"labelLaGroup\",\"checked\":false,\"authRegular\":\"/admin/tag-invite.*\",\"children\":[]},{\"title\":\"群列表\",\"identifier\":\"groupList\",\"checked\":false,\"authRegular\":\"/admin/external-group.*\",\"children\":[]},{\"title\":\"群码管理\",\"identifier\":\"GroupCodeManagement\",\"checked\":false,\"authRegular\":\"/admin/guide/group-qrcode.*\",\"children\":[]},{\"title\":\"群任务\",\"identifier\":\"groupTask\",\"checked\":false,\"authRegular\":\"/admin/group-task.*\",\"children\":[]}]},{\"title\":\"客户CRM\",\"identifier\":\"customerCRM\",\"checked\":false,\"authRegular\":\"/admin.*\",\"children\":[{\"title\":\"企微客户\",\"identifier\":\"enterpriseClient\",\"checked\":false,\"authRegular\":\"/admin/customer.*\",\"children\":[]},{\"title\":\"流失客户\",\"identifier\":\"lossCustomers\",\"checked\":false,\"authRegular\":\"/admin.*\",\"children\":[]}]}]},{\"title\":\"店铺\",\"identifier\":\"shop\",\"checked\":true,\"authRegular\":\"/admin.*\",\"children\":[{\"title\":\"门店\",\"identifier\":\"branch\",\"checked\":true,\"authRegular\":\"/admin/shop.*\",\"children\":[{\"title\":\"门店管理\",\"identifier\":\"branchList\",\"checked\":true,\"authRegular\":\"/admin/shop.*\",\"children\":[]},{\"title\":\"导购管理\",\"identifier\":\"shopGuideList\",\"checked\":false,\"authRegular\":\"/admin/shoppingguide.*\",\"children\":[]},{\"title\":\"目标设置\",\"identifier\":\"targetSetting\",\"checked\":false,\"authRegular\":\"/admin/guide/guide-goals.*\",\"children\":[]},{\"title\":\"奖金设置\",\"identifier\":\"bonusSetting\",\"checked\":false,\"authRegular\":\"/admin/guide/guide-bonuss.*\",\"children\":[]}]},{\"title\":\"导购统计\",\"identifier\":\"shopGuideStatics\",\"checked\":false,\"authRegular\":\"/admin/guide/performances.*\",\"children\":[{\"title\":\"导购统计\",\"identifier\":\"shopGuideStatic\",\"checked\":false,\"authRegular\":\"/admin/guide/performances.*\",\"children\":[]},{\"title\":\"业绩统计\",\"identifier\":\"performanceDetail1\",\"checked\":false,\"authRegular\":\"/admin/guide/performances.*\",\"children\":[]},{\"title\":\"佣金明细\",\"identifier\":\"guideCommissionDetail\",\"checked\":false,\"authRegular\":\"/admin/guide/performances.*\",\"children\":[]},{\"title\":\"单聊统计\",\"identifier\":\"guideChatStatistics\",\"checked\":false,\"authRegular\":\"/admin/guide/single/chat.*\",\"children\":[]}]},{\"title\":\"加盟\",\"identifier\":\"performances\",\"checked\":false,\"authRegular\":\"/admin/guide.*\",\"children\":[{\"title\":\"加盟商管理\",\"identifier\":\"franchiseList\",\"checked\":false,\"authRegular\":\"/admin/guide/performances.*\",\"children\":[]},{\"title\":\"加盟商结算\",\"identifier\":\"settlement\",\"checked\":false,\"authRegular\":\"/admin/guide/performances.*\",\"children\":[]}]}]},{\"title\":\"运维\",\"identifier\":\"maintenance\",\"checked\":false,\"authRegular\":\"/maintenance\",\"children\":[{\"title\":\"商品\",\"identifier\":\"goods\",\"checked\":false,\"authRegular\":\"/admin/goods.*\",\"children\":[{\"title\":\"商品列表\",\"identifier\":\"goodsList\",\"checked\":false,\"authRegular\":\"(/admin/goods.*)|(/admin/shops/list)\",\"children\":[]},{\"title\":\"分类\",\"identifier\":\"categoryList\",\"checked\":false,\"authRegular\":\"(/admin/goods/categories.*)|(/admin/goods/parameters.*)|(/admin/goods/parameter-groups.*)\",\"children\":[]},{\"title\":\"品牌\",\"identifier\":\"brandList\",\"checked\":false,\"authRegular\":\"/admin/goods/brands.*\",\"children\":[]},{\"title\":\"规格\",\"identifier\":\"specList\",\"checked\":false,\"authRegular\":\"/admin/goods/specs.*\",\"children\":[]}]},{\"title\":\"订单\",\"identifier\":\"order\",\"checked\":false,\"authRegular\":\"/admin/trade.*\",\"children\":[{\"title\":\"订单列表\",\"identifier\":\"orderList\",\"checked\":false,\"authRegular\":\"(/admin/trade/orders.*)|(/admin/shops.*)\",\"children\":[]},{\"title\":\"售后服务\",\"identifier\":\"serviceList\",\"checked\":false,\"authRegular\":\"/admin/after-sales.*\",\"children\":[]},{\"title\":\"投诉处理\",\"identifier\":\"complaintList\",\"checked\":false,\"authRegular\":\"/admin/trade/order-complains.*\",\"children\":[]},{\"title\":\"投诉主题\",\"identifier\":\"subjectList\",\"checked\":false,\"authRegular\":\"/admin/systems/complain-topics.*\",\"children\":[]},{\"title\":\"咨询\",\"identifier\":\"goodsAskList\",\"checked\":false,\"authRegular\":\"/admin/members/asks.*\",\"children\":[]},{\"title\":\"评价\",\"identifier\":\"goodsCommentList\",\"checked\":false,\"authRegular\":\"/admin/members/comments.*\",\"children\":[]}]},{\"title\":\"会员\",\"identifier\":\"member\",\"checked\":false,\"authRegular\":\"/admin/members.*\",\"children\":[{\"title\":\"会员列表\",\"identifier\":\"memberList\",\"checked\":false,\"authRegular\":\"(/admin/members.*)|(/admin/trade/orders.*)\",\"children\":[]},{\"title\":\"会员等级\",\"identifier\":\"memberLevel\",\"checked\":false,\"authRegular\":\"/admin/members/grade.*\",\"children\":[]},{\"title\":\"会员回收站\",\"identifier\":\"memberRecycle\",\"checked\":false,\"authRegular\":\"/admin/members.*\",\"children\":[]},{\"title\":\"会员标签\",\"identifier\":\"labelManage\",\"checked\":false,\"authRegular\":\"/admin/tag.*\",\"children\":[]},{\"title\":\"标签分组\",\"identifier\":\"labelGroupManage\",\"checked\":false,\"authRegular\":\"/admin/tag/group.*\",\"children\":[]},{\"title\":\"专票资质\",\"identifier\":\"receiptExamine\",\"checked\":false,\"authRegular\":\"/admin/members/zpzz.*\",\"children\":[]}]},{\"title\":\"直播\",\"identifier\":\"liveVideo\",\"checked\":false,\"authRegular\":\"zhibo\",\"children\":[{\"title\":\"直播列表\",\"identifier\":\"liveVideoList\",\"checked\":false,\"authRegular\":\"liveVideoList\",\"children\":[]},{\"title\":\"直播商品\",\"identifier\":\"liveVideoGoodsList\",\"checked\":false,\"authRegular\":\"liveVideoGoodsList\",\"children\":[]}]},{\"title\":\"促销\",\"identifier\":\"promotions\",\"checked\":false,\"authRegular\":\"/admin/promotion.*\",\"children\":[{\"title\":\"优惠劵\",\"identifier\":\"couponList\",\"checked\":false,\"authRegular\":\"/admin/promotion/coupons.*\",\"children\":[]},{\"title\":\"膨胀券\",\"identifier\":\"Inflation\",\"checked\":false,\"authRegular\":\"/admin/swell.*\",\"children\":[]},{\"title\":\"满减满赠\",\"identifier\":\"fullCut\",\"checked\":false,\"authRegular\":\"/admin/promotion.*\",\"children\":[]},{\"title\":\"赠品\",\"identifier\":\"giftManager\",\"checked\":false,\"authRegular\":\"/admin/promotion.*\",\"children\":[]},{\"title\":\"单品立减\",\"identifier\":\"singleCut\",\"checked\":false,\"authRegular\":\"/admin/promotion.*\",\"children\":[]},{\"title\":\"第二件半价\",\"identifier\":\"secondHalfPrice\",\"checked\":false,\"authRegular\":\"/admin/promotion.*\",\"children\":[]},{\"title\":\"限时抢购\",\"identifier\":\"seckillList\",\"checked\":false,\"authRegular\":\"/admin/promotion.*\",\"children\":[]},{\"title\":\"拼团活动\",\"identifier\":\"assembleList\",\"checked\":false,\"authRegular\":\"(/admin/promotion/pintuan.*)|(/admin/shops/list)\",\"children\":[]},{\"title\":\"团购列表\",\"identifier\":\"groupBuyList\",\"checked\":false,\"authRegular\":\"/admin/promotion.*\",\"children\":[]},{\"title\":\"团购分类\",\"identifier\":\"groupBuyCategory\",\"checked\":false,\"authRegular\":\"/admin/promotion/group-buy-cats.*\",\"children\":[]},{\"title\":\"积分商品\",\"identifier\":\"pointsGoods\",\"checked\":false,\"authRegular\":\"/admin/goods.*\",\"children\":[]},{\"title\":\"积分分类\",\"identifier\":\"pointsClassify\",\"checked\":false,\"authRegular\":\"/admin/promotion/exchange-cats.*\",\"children\":[]}]},{\"title\":\"权限\",\"identifier\":\"authSettings\",\"checked\":false,\"authRegular\":\"admin/system.*\",\"children\":[{\"title\":\"管理员\",\"identifier\":\"administratorManage\",\"checked\":false,\"authRegular\":\"(/admin/systems/roles.*)\",\"children\":[]},{\"title\":\"角色\",\"identifier\":\"roleManage\",\"checked\":false,\"authRegular\":\"(/admin/systems/menus.*)\",\"children\":[]}]},{\"title\":\"财务\",\"identifier\":\"finance\",\"checked\":false,\"authRegular\":\"/finance\",\"children\":[{\"title\":\"收款单\",\"identifier\":\"collectionList\",\"checked\":false,\"authRegular\":\"/admin/trade/orders/pay-log.*\",\"children\":[]},{\"title\":\"开票历史\",\"identifier\":\"receiptHistory\",\"checked\":false,\"authRegular\":\"/admin/members/receipts.*\",\"children\":[]},{\"title\":\"预存款列表\",\"identifier\":\"preDepositDetail\",\"checked\":false,\"authRegular\":\"/admin/members/deposit.*\",\"children\":[]},{\"title\":\"退款单\",\"identifier\":\"refundList\",\"checked\":false,\"authRegular\":\"/admin/after-sales.*\",\"children\":[]}]}]},{\"title\":\"进销存\",\"identifier\":\"inventory\",\"checked\":false,\"authRegular\":\"/inventory\",\"children\":[{\"title\":\"库存\",\"identifier\":\"stock\",\"checked\":false,\"authRegular\":\"/admin/stock.*\",\"children\":[{\"title\":\"库存查询\",\"identifier\":\"stockList\",\"checked\":false,\"authRegular\":\"admin/stock.*\",\"children\":[]},{\"title\":\"库存流水\",\"identifier\":\"stockDetail\",\"checked\":false,\"authRegular\":\"admin/stock.*\",\"children\":[]},{\"title\":\"入库管理\",\"identifier\":\"inStorage\",\"checked\":false,\"authRegular\":\"admin/storage.*\",\"children\":[]},{\"title\":\"出库管理\",\"identifier\":\"outStorage\",\"checked\":false,\"authRegular\":\"admin/storage.*\",\"children\":[]},{\"title\":\"库存盘点\",\"identifier\":\"checkManage\",\"checked\":false,\"authRegular\":\"admin/check.*\",\"children\":[]},{\"title\":\"门店调拨\",\"identifier\":\"transferManage\",\"checked\":false,\"authRegular\":\"admin/transfer.*\",\"children\":[]}]},{\"title\":\"采购\",\"identifier\":\"purchase\",\"checked\":false,\"authRegular\":\"/admin/purchase.*\",\"children\":[{\"title\":\"采购订单\",\"identifier\":\"purchaseList\",\"checked\":false,\"authRegular\":\"/admin/purchase.*\",\"children\":[]},{\"title\":\"采购退货\",\"identifier\":\"purchaseReturn\",\"checked\":false,\"authRegular\":\"/admin/purchase-return.*\",\"children\":[]},{\"title\":\"采购对账\",\"identifier\":\"accountStatement\",\"checked\":false,\"authRegular\":\"/admin/accountstatements.*\",\"children\":[]},{\"title\":\"供应商管理\",\"identifier\":\"supplierManage\",\"checked\":false,\"authRegular\":\"/admin/purchase/supplier.*\",\"children\":[]},{\"title\":\"门店要货\",\"identifier\":\"requireManage\",\"checked\":false,\"authRegular\":\"/admin/require.*\",\"children\":[]},{\"title\":\"要货退货\",\"identifier\":\"requireReturn\",\"checked\":false,\"authRegular\":\"/admin/requirereturns.*\",\"children\":[]}]},{\"title\":\"存货\",\"identifier\":\"stocks\",\"checked\":false,\"authRegular\":\"/stocks\",\"children\":[{\"title\":\"保质期预警\",\"identifier\":\"warrantyWarning\",\"checked\":false,\"authRegular\":\"/admin/warning.*\",\"children\":[]},{\"title\":\"存货明细\",\"identifier\":\"remainderRecord\",\"checked\":false,\"authRegular\":\"/admin/inventory/detail.*\",\"children\":[]},{\"title\":\"批次管理\",\"identifier\":\"batchManage\",\"checked\":false,\"authRegular\":\"/admin/batch.*\",\"children\":[]},{\"title\":\"均价管理\",\"identifier\":\"avgChangeRecord\",\"checked\":false,\"authRegular\":\"/admin/avg/change\",\"children\":[]}]}]}]");
        return roleDO;
    }


    /**
     * 筛选checked为true的菜单
     *
     * @param menuList 菜单集合
     */
    private void reset(List<Menus> menuList, List<String> authList) {
        for (Menus menus : menuList) {
            //将此角色拥有的菜单权限放入list中
            if (menus.isChecked()) {
                authList.add(menus.getIdentifier());
            }
            if (!menus.getChildren().isEmpty()) {
                reset(menus.getChildren(), authList);
            }
        }
    }

    /**
     * 获取角色表
     *
     * @param id 角色表主键
     * @return Role  角色表
     */
    @Override
    public RoleVO getRole(Long id) {
        return new RoleVO(this.getModel(id));
    }
}

