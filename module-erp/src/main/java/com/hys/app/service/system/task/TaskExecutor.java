package com.hys.app.service.system.task;

import com.hys.app.model.system.dos.TaskDO;

/**
 * 任务执行器
 *
 * @author 张崧
 * 2023-05-12
 */
public interface TaskExecutor {

    /**
     * 执行
     *
     * @param task
     * @param function
     * @param checkRunning
     */
    void execute(TaskDO task, TaskRunnable function, boolean checkRunning);

    /**
     * 终止（目前仅支持单机环境）
     *
     * @param taskId
     */
    void stop(Long taskId);

}
