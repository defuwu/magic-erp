package com.hys.app.controller.open;

import com.hys.app.model.oauth2.dto.LoginClient;
import com.hys.app.model.oauth2.vo.OAuth2OpenAuthorizeInfoRespVO;
import com.hys.app.oauth2.LoginClientHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * OAuth2.0 授权 API
 *
 * @author 张崧
 * @since 2024-02-20
 */
@Api(tags = "OAuth2.0 授权")
@RestController
@RequestMapping("/open/test")
@Validated
@Slf4j
public class TestController {

    @GetMapping
    public String test() {
        return LoginClientHolder.getClientId();
    }

}
