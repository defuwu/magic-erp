package com.hys.app.model.datasync.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.datasync.enums.MessageReceiveStatusEnum;
import com.hys.app.model.datasync.enums.MessageReceiveTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 消息接收实体类
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
@TableName("erp_message_receive")
@Data
public class MessageReceiveDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "type", value = "消息类型")
    private MessageReceiveTypeEnum type;

    @ApiModelProperty(name = "msg_id", value = "外部系统的消息id")
    private String msgId;

    /**
     * todo 大文本字段单独存一张表
     */
    @ApiModelProperty(name = "content", value = "消息内容")
    private String content;

    @ApiModelProperty(name = "produce_time", value = "外部系统生成消息的时间")
    private Long produceTime;

    @ApiModelProperty(name = "receive_time", value = "接收消息的时间")
    private Long receiveTime;

    @ApiModelProperty(name = "status", value = "状态")
    private MessageReceiveStatusEnum status;

    @ApiModelProperty(name = "remark", value = "处理结果备注")
    private String remark;

}
